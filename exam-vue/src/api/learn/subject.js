import { post } from '@/utils/request'
/**
 * 获取课程详情
 */
export function getSubject(current, size, id, courseName, courseCategory) {
  return post('/course/api/user/course/courseDetails', {
    current: current,
    size: size,
    params: {
      id: id,
      courseName: courseName,
      courseCategory: courseCategory
    }

  })
}
/**
 * 获取课程目录
 */
export function getCatalogue(userID, courseID) {
  return post('/course/api/course/course/courseInfo/catalogue', {
    userID: userID,
    courseID: courseID
  })
}

/**
 * 保存提问问题
 */
export function saveQuestion(courseId, coursewareId, content, title) {
  return post('/exam/api/qa/save', {
    courseId: courseId,
    coursewareId: coursewareId,
    content: content,
    title: title
  })
}
/**
 * 问题列表
 */
export function getQuestionList(current, size, courseId, coursewareId) {
  return post('/exam/api/qa/detail', {
    current: current,
    size: size,
    params: {
      courseId: courseId,
      coursewareId: coursewareId
    }
  })
}
/**
 * 搜索框
 */
export function getSelect(current, size) {
  return post('/course/api/user/course/courseUserCategorySearch', {
    current: current,
    size: size

  })
}
/**
 * 倒计时
 */
export function getTime(userID, courseID, courseEmployTime) {
  return post('/course/api/course/course/courseEmployTime/update', {
    userID: userID,
    courseID: courseID,
    courseEmployTime: courseEmployTime
  })
}

/**
 * 更新课件使用时长
 */
export function updatingCourseware(userId, courseId, coursewareEmployTime, coursewareId) {
  return post('/course/api/course/course/coursewareEmployTime/update', {
    userId: userId,
    courseId: courseId,
    coursewareEmployTime: coursewareEmployTime,
    coursewareId: coursewareId
  })
}

/**
 * 答复问题
 */
export function getAnswer(id, answer) {
  return post('/exam/api/qa/updata', {
    id: id,
    answer: answer
  })
}

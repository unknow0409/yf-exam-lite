import { post } from '@/utils/request'

export function userStat(data) {
  return post('/exam/api/stat/stat/paging', data)
}

export function getTable(current, size, startTime, endTime) {
  return post('/exam/api/stat/stat/paging', {
    current: current,
    size: size,
    params: {
      startTime: startTime,
      endTime: endTime
    }
  })
}

import { post } from '@/utils/request'

export function addCourseware(data) {
  return post('/course/api/course/course//courseware/save ', {
    params: data
  })
}

export function delectCourseware(id) {
  return post('/course/api/course/course/courseware/delete', {
    ids: id
  })
}

export function getCourseware(id) {
  return post('/course/api/course/course/courseware/entity', {
    id: id
  })
}

export function getCoursewareType() {
  return post('/course/api/course/course/category-detail', {
  })
}

export function getCoursewareClassify() {
  return post('/course/api/course/course/courseware/category-detail', {
  })
}

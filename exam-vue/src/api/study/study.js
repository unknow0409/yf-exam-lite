import { post } from '@/utils/request'

export function addList(data) {
  return post('/course/api/course/course/save', {
    params: data
  })
}

export function getList(id) {
  return post('/course/api/course/course/search_course', {
    id: id
  })
}

export function getSearch(id) {
  return post('/course/api/course/course/courseSearch', {
    id: id
  })
}

export function delectCourse(id) {
  return post('/course/api/course/course/delete', {
    ids: id
  })
}

export function getQuestionSearch() {
  return post('/course/api/course/course/courseSearch', {
  })
}

export function delectQuestion(id) {
  return post('/exam/api/qa/delete', {
    ids: id
  })
}

export function getForm() {
  return post('/course/api/course/course/courseSearch', {
  })
}

export function saveForm(id, answer) {
  return post('/exam/api/qa/updata', {
    id: id,
    answer: answer
  })
}

export function getCourseware(current, size) {
  return post('/course/api/course/course/search_coursewareDetails', {
    size: size,
    params: {},
    current: current
  })
}


import { post } from '@/utils/request'

export function save(id, catalogName, catalogCode, description) {
  return post('/catalog/api/catalog/catalog/save', {
    id: id,
    catalogName: catalogName,
    catalogCode: catalogCode,
    description: description
  })
}
export function delect(id) {
  return post('/catalog/api/catalog/catalog/delete', {
    ids: id
  })
}

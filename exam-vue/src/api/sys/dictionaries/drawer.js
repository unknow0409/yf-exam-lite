import { post } from '@/utils/request'

export function select(id, code) {
  return post('/exam/api/catalog/son/data', {
    catalogId: id,
    catalogCode: code
  })
}
export function add(id, parentId, catalogSonName, catalogSonDescription, catalogSonCode) {
  return post('/exam/api/catalog/son/insert', {
    catalogId: id,
    parentId: parentId,
    catalogSonName: catalogSonName,
    catalogSonDescription: catalogSonDescription,
    catalogSonCode: catalogSonCode
  })
}

export function save(id, catalogSonName, catalogSonDescription, catalogSonCode) {
  return post('/exam/api/catalog/son/updata', {
    Id: id,
    catalogSonName: catalogSonName,
    catalogSonDescription: catalogSonDescription,
    catalogSonCode: catalogSonCode
  })
}
export function delect(id) {
  return post('/exam/api/catalog/son/delete', {
    id: id
  })
}

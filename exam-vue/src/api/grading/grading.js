import { post } from '@/utils/request'

export function findExamAnswer(data) {
  return post('/exam/api/exam/exam/findExamAnswer', data)
}
/**
 * 修改分数
 * @param data
 */
export function updateScore(data) {
  return post('/exam/api/exam/exam/updatePaperScore', data)
}

/**
 * 修改成绩及试卷状态
 */
export function updateUserScore(data) {
  return post('/exam/api/exam/exam/updateUserScore', data)
}

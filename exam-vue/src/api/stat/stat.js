
import { post } from '@/utils/request'

/**
 * 获取搜索
 */
export function getExam() {
  return post('/exam/api/exam/exam/search')
}

/**
 * 获取表格内容
 */
export function getTable(current, size, examName, startTime, endTime, subject) {
  return post('/exam/api/exam/exam/examStatistics', {
    current: current,
    size: size,
    params: {
      examName: examName,
      startTime: startTime,
      endTime: endTime,
      subject: subject
    }
  })
}

/**
 * 获取考试详情
  *
 */
export function getName(examDate, subject) {
  return post('/exam/api/exam/exam/examDetailsSearch', {
    examDate: examDate,
    subject: subject
  })
}

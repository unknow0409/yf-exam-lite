
import videojs from 'video.js'
import video_zhCN from 'video.js/dist/lang/zh-CN.json'
import video_en from 'video.js/dist/lang/en.json'
import 'video.js/dist/video-js.css'

videojs.addLanguage('zh-CN', video_zhCN)
videojs.addLanguage('en', video_en)

const videoUtils = {
  videojs: videojs,
  // 全局默认值
  defaultOptions: {
    autoplay: 'muted', // 自动播放
    controls: true, // 用户可以与之交互的控件
    controlBar: { // 设置控制条组件
      children: [
        { name: 'playToggle' }, // 播放按钮
        { name: 'progressControl' }, // 播放进度条
        { name: 'currentTimeDisplay' }, // 当前已播放时间
        { name: 'timeDivider' }, // 显示时间分割线
        { name: 'durationDisplay' }, // 总时间
        { name: 'playbackRateMenuButton' }, // 倍数播放
        { name: 'volumePanel', inline: false }, // 音量控制
        { name: 'FullscreenToggle' } // 全屏
      ]
    },
    playbackRates: [2.0, 1.5, 1.0], // 切换播放速率按钮
    loop: false, // 视频一结束就重新开始
    muted: false, // 默认情况下将使所有音频静音
    aspectRatio: '16:9', // 显示比率
    fluid: true, // 自适应宽高
    language: 'zh-CN', // 设置语言
    fullscreen: {
      options: { navigationUI: 'hide' }
    },
    poster: '', // 视频封面图地址
    sources: [] // 视频源
  }
}
export default videoUtils

package com.yf.exam.modules.user.course.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author lgy
 * @title :CourseUserDetailsReqDTO
 * @projectName mylite
 * @Data 2021/8/19 15:20
 */
@Data
@ApiModel(value="课程查询请求类", description="课程查询请求类")
public class CourseUserDetailsReqDTO {

    @ApiModelProperty(value = "用户ID")
    private String id;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "课程类别")
    private String courseCategory;
    }

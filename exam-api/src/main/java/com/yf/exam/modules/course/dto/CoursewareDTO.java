package com.yf.exam.modules.course.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName CoursewareDTO
 * @Description 课件数据传输类
 * @Author BlueAutumn
 * @Date 2021/8/19 14:58
 * @Version 1.0
 */
@Data
@ApiModel(value = "课件",description = "课件")
public class CoursewareDTO implements Serializable {

    private static final long serialVersionUID = -5959372298949791405L;

    @ApiModelProperty(value = "课件ID", required = true)
    private String id;

    @ApiModelProperty(value = "课件名称", required = true)
    private String coursewareName;

    @ApiModelProperty(value = "课件类型", required = true)
    private String coursewareCategory;

    @ApiModelProperty(value = "课程类目", required = true)
    private String courseCategory;

    @ApiModelProperty(value = "课件创建时间", required = true)
    private Date createTime;

    @ApiModelProperty(value = "课件修改时间", required = true)
    private Date changeTime;

    @ApiModelProperty(value = "课件链接", required = true)
    private String address;

    @ApiModelProperty(value = "文件转换后的路径", required = true)
    private String viewUrl;

}

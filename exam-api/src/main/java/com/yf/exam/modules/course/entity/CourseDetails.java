package com.yf.exam.modules.course.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 课程详情返回类
 *
 * @author lgy
 * @title :CourseDetails
 * @projectName mylite
 * @Data 2021/8/20 14:37
 */
public class CourseDetails {

    /**
     * 用户名称
     */
    @TableField("user_name")
    private String userName;

    /**
     * 课程名
     */
    @TableField("course_name")
    private String courseName;

    /**
     * 已学课件数
     */
    private Integer studiedCourse;

    /**
     * 总学课件数
     */
    private Integer totalCourse;

    /**
     * 学习状态
     */
    private String learningState;

    /**
     * 更新时间
     */
    @TableField("change_time")
    private Data changeTime;

}

package com.yf.exam.modules.course.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName Courseware
 * @Description 课件实体类
 * @Author BlueAutumn
 * @Date 2021/8/19 14:50
 * @Version 1.0
 */
@Data
@TableName("el_courseware")
public class Courseware extends Model<Courseware> {

    private static final long serialVersionUID = 8315094213106905351L;

    /**
     * 课件ID
     */
    @TableId(value = "ID",type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 课件名称
     */
    @TableField("courseware_name")
    private String coursewareName;

    /**
     * 课件类型
     */
    @TableField("courseware_category")
    private String coursewareCategory;

    /**
     * 课程类目
     */
    @TableField("course_category")
    private String courseCategory;

    /**
     * 课件创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 课件修改时间
     */
    @TableField("change_time")
    private Date changeTime;

    /**
     * 课件连接
     */
    private String address;
    /**
     * 文件转换后的路径
     */
    @TableField("view_url")
    private String viewUrl;



}

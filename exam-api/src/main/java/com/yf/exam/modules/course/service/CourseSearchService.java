package com.yf.exam.modules.course.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.course.dto.CourseDTO;
import com.yf.exam.modules.course.entity.Course;

import java.util.List;

/**
 * @author lgy
 * @title :CourseSearchService
 * @projectName mylite
 * @Data 2021/8/20 8:56
 */
public interface CourseSearchService extends IService<Course> {

    List<String> courseSearch(CourseDTO query);
}

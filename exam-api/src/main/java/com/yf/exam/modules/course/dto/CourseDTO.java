package com.yf.exam.modules.course.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName CourseDTO
 * @Description 课程数据传输类
 * @Author BlueAutumn
 * @Date 2021/8/19 10:43
 * @Version 1.0
 */
@Data
@ApiModel(value = "课程", description = "课程")
public class CourseDTO implements Serializable {

    private static final long serialVersionUID = 4724044288921945067L;

    @ApiModelProperty(value = "课程ID", required = true)
    private String id;

    @ApiModelProperty(value = "课程名称", required = true)
    private String courseName;

    @ApiModelProperty(value = "课程封面地址", required = true)
    private String img;

    @ApiModelProperty(value = "课程简介", required = true)
    private String description;

    @ApiModelProperty(value = "课程分类", required = true)
    private String courseCategory;

    @ApiModelProperty(value = "课时", required = true)
    private Double courseTime;

    @ApiModelProperty(value = "课程创建时间", required = true)
    private Date createTime;

    @ApiModelProperty(value = "课程修改时间", required = true)
    private Date changeTime;

    @ApiModelProperty(value = "课程积分", required = true)
    private Double courseIntegral;

    @ApiModelProperty(value = "课程是否必修，0非必修,1必修", required = true)
    private Integer isNecessary;

    @ApiModelProperty(value = "课程开放权限，1完全开放，2指定用户开放", required = true)
    private Integer openType;

}

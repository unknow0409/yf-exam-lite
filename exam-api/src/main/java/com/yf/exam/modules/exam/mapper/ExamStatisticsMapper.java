package com.yf.exam.modules.exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yf.exam.modules.exam.dto.ExamStatisticsDTO;
import com.yf.exam.modules.exam.dto.request.ExamStatisticsReqDTO;
import com.yf.exam.modules.exam.entity.ExamStatistics;
import org.apache.ibatis.annotations.Param;

/**
 * @author lgy
 * @Data2021/8/11 14:23
 */
public interface ExamStatisticsMapper extends BaseMapper<ExamStatistics> {


    IPage<ExamStatisticsDTO> statistics(Page page, @Param("query") ExamStatisticsReqDTO query);


}

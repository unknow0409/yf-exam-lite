package com.yf.exam.modules.exam.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author lgy
 * @Data2021/8/12 16:39
 */
@Data
@ApiModel(value = "考试数据统计",description = "考试数据统计")
public class ExamStatisticsDTO  implements Serializable {

    @ApiModelProperty(value = "科目")
    private String subject;

    @ApiModelProperty(value = "考试时间")
    private String date;

    @ApiModelProperty(value = "考试人次")
    private String examNum;

    @ApiModelProperty(value = "考生人数")
    private  String numofPeople;

    @ApiModelProperty(value = "通过人数")
    private String passNum;

    @ApiModelProperty(value = "通过率")
    private  String passPro;

}

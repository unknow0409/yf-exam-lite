package com.yf.exam.modules.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.core.utils.BeanMapper;
import com.yf.exam.core.utils.StringUtils;
import com.yf.exam.modules.course.dto.CourseDTO;
import com.yf.exam.modules.course.dto.request.*;
import com.yf.exam.modules.course.dto.response.CourseCatalogueRespDTO;
import com.yf.exam.modules.course.dto.response.CourseInfoRespDTO;
import com.yf.exam.modules.course.dto.response.CoursewareVideoInfoRespDTO;
import com.yf.exam.modules.course.entity.*;
import com.yf.exam.modules.course.mapper.CourseMapper;
import com.yf.exam.modules.course.service.CourseCatalogueService;
import com.yf.exam.modules.course.service.CourseService;
import com.yf.exam.modules.course.service.CoursewareService;
import org.springframework.beans.factory.annotation.Autowired;
import com.yf.exam.modules.course.service.*;
import com.yf.exam.modules.enums.OpenType;
import com.yf.exam.modules.sys.depart.entity.SysDepart;
import com.yf.exam.modules.sys.depart.service.SysDepartService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



/**
 * @ClassName CourseServiceImpl
 * @Description 课程业务实现类
 * @Author BlueAutumn
 * @Date 2021/8/19 10:58
 * @Version 1.0
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements CourseService {

    @Autowired
    private CourseDepartService courseDepartService;

    @Autowired
    private CourseCatalogueService courseCatalogueService;

    @Autowired
    private CatalogueCoursewareService catalogueCoursewareService;

    @Autowired
    private CourseCoursewareService courseCoursewareService;

    @Autowired
    private SysDepartService departService;

    @Autowired
    private CoursewareService coursewareService;

    @Autowired
    private CourseRecordService courseRecordService;

    @Override
    public IPage<CourseDTO> paging(PagingReqDTO<CoursePagReqDTO> reqDTO) {

        // 创建分页对象
        Page page = new Page<>(reqDTO.getCurrent(), reqDTO.getSize());

        // 转换结果
        IPage<CourseDTO> pageData = baseMapper.paging(page, reqDTO.getParams());
        return pageData;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void save(CourseSaveReqDTO reqDTO) {

        // ID
        String id = reqDTO.getId();

        // 复制参数
        Course course = new Course();

        // 复制基本数据
        BeanMapper.copy(reqDTO, course);
        // 更新时间
        if (StringUtils.isBlank(id)) {
            id = IdWorker.getIdStr();
            course.setCreateTime(new Date(new Date().getTime()));
        } else {
            course.setChangeTime(new Date(new Date().getTime()));
        }
        course.setId(id);

        // 开放的部门
        if (OpenType.DEPT_OPEN.equals(course.getOpenType())) {
            courseDepartService.saveAll(id, reqDTO.getDepartIds());
        }

        // 目录课件信息和课程课件信息
        if (reqDTO.getCatalogueInfoList() != null) {
            courseCatalogueService.saveAll(id, reqDTO.getCatalogueInfoList());

            // 目录保存完毕之后，开始保存目录的信息
            // 单独保存课程课件的信息
            // 先获取所有课件信息
            List<CoursewareInfoReqDTO> coursewareList = new ArrayList<>();
            for (CatalogueInfosReqDTO catalogueInfo : reqDTO.getCatalogueInfoList()) {
                for (int i = 0; i < catalogueInfo.getCoursewareInfoList().size(); i++) {
                    coursewareList.add(catalogueInfo.getCoursewareInfoList().get(i));
                }
            }
            if (coursewareList != null && !coursewareList.isEmpty()) {
                courseCoursewareService.saveAll(id, coursewareList);
            }
            catalogueCoursewareService.saveAll(id, reqDTO.getCatalogueInfoList());
        }

        this.saveOrUpdate(course);
    }

    @Override
    public CourseInfoRespDTO getInfoById(String courseId) {

        CourseInfoRespDTO dto = new CourseInfoRespDTO();

        // 获取课程信息并复制
        Course course = baseMapper.selectById(courseId);
        BeanUtils.copyProperties(course, dto);

        // 获取部门信息并复制
        List<String> departIds = baseMapper.getDepartIdsByCourseId(courseId);
        List<SysDepart> departList = new ArrayList<>();
        if (departIds != null && !departIds.isEmpty()) {
            departList = departService.listByIds(departIds);
        }
        dto.setDepartList(departList);

        // 获取目录信息并复制
        List<CatalogueInfosReqDTO> catalogueInfosReqDTOList = new ArrayList<>();
        List<String> catalogueIds = baseMapper.getCatalogueIdsByCourseId(courseId);
        List<CourseCatalogue> catalogueList = new ArrayList<>();
        if (catalogueIds != null && !catalogueIds.isEmpty()) {
            catalogueList = courseCatalogueService.listByIds(catalogueIds);
            for (CourseCatalogue courseCatalogue : catalogueList) {
                CatalogueInfosReqDTO catalogueInfosReqDTO = new CatalogueInfosReqDTO();
                BeanUtils.copyProperties(courseCatalogue, catalogueInfosReqDTO);
                catalogueInfosReqDTO.setCoursewareInfoList(coursewareService.getInfoByCatalogueId(courseCatalogue.getId()));
                catalogueInfosReqDTOList.add(catalogueInfosReqDTO);
            }
        }
        dto.setCatalogueInfoList(catalogueInfosReqDTOList);

        return dto;
    }

    @Override
    public void removeBatchByIds(List<String> courseIds) {
        // 删除课程权限的部门
        for (String courseId : courseIds) {
            QueryWrapper<CourseDepart> departQueryWrapper = new QueryWrapper<>();
            departQueryWrapper.lambda().eq(CourseDepart::getCourseId, courseId);
            courseDepartService.remove(departQueryWrapper);
        }


        // 删除课程下的课件
        for (String courseId : courseIds) {
            QueryWrapper<CourseCourseware> courseCoursewareQueryWrapper = new QueryWrapper<>();
            courseCoursewareQueryWrapper.lambda().eq(CourseCourseware::getCourseId, courseId);
            courseCoursewareService.remove(courseCoursewareQueryWrapper);
        }

        // 删除课程目录下的课件
        for (String courseId : courseIds) {
            List<String> catalogueIds = courseCatalogueService.idListByCourseId(courseId);
            for (String catalogueId : catalogueIds) {
                QueryWrapper<CatalogueCourseware> catalogueCoursewareQueryWrapper = new QueryWrapper<>();
                catalogueCoursewareQueryWrapper.lambda().eq(CatalogueCourseware::getCatalogueId, catalogueId);
                catalogueCoursewareService.remove(catalogueCoursewareQueryWrapper);
            }
            // 删除课程下的目录
            courseCatalogueService.removeByIds(catalogueIds);
        }
        // 删除课程
        this.removeByIds(courseIds);
    }

    /**
     *获取课程的详细信息及目录列表
     * @param reqDTO
     * @return
     */
    @Override
    public CoursewareVideoInfoRespDTO findCourseAndCatalogue(CourseUserIDReqDTO reqDTO) {
        CoursewareVideoInfoRespDTO respDTO=new CoursewareVideoInfoRespDTO();
        //课程信息---目录的信息---课件的信息

        //判断用户学习总课时统计表是否为空
            //为空时，添加课程记录
        QueryWrapper<CourseRecord> wrapper=new QueryWrapper<>();

        wrapper.lambda().eq(CourseRecord::getCourseId,reqDTO.getCourseID())
                .eq(CourseRecord::getUserId,reqDTO.getUserID());

        if(courseRecordService.getOne(wrapper)==null){
            CourseRecord courseRecord=new CourseRecord();
            courseRecord.setUserId(reqDTO.getUserID());
            courseRecord.setCourseId(reqDTO.getCourseID());
            courseRecord.insert();
        }
        //获取课程的信息
        respDTO=baseMapper.findCourseTimeOneInfo(reqDTO);
        respDTO.setCourseSurplusTime((int) (respDTO.getCourseTime()*60*60-respDTO.getCourseEmployTime()));
        //获取对应课程的目录
        List<CourseCatalogueRespDTO> dirList = courseCatalogueService.findList(reqDTO);
        //获取目录下的课件
        for (int i = 0; i <dirList.size() ; i++) {
            reqDTO.setCatalogueID(dirList.get(i).getId());
            dirList.get(i).setFileList(coursewareService.findCoursewareseTime(reqDTO));
        }
        respDTO.setDirList(dirList);
        return respDTO;
    }
}

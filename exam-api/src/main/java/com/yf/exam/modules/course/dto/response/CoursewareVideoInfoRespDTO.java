package com.yf.exam.modules.course.dto.response;

import com.yf.exam.modules.course.entity.Course;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName CoursewareVideoInfoRespDTO
 * @Description 课程课件目录的详细信息响应
 * @Author Mr.Fu
 * @Date 2021-08-20 14:06:22
 * @Version 1.0
 */
@Data
public class CoursewareVideoInfoRespDTO extends Course {

    @ApiModelProperty(value = "课程总时长的使用时间", required=true)
    private int courseEmployTime;
    @ApiModelProperty(value = "课程总时长的剩余时间", required=true)
    private int courseSurplusTime;
    @ApiModelProperty(value = "目录的信息", required=true)
    private List<CourseCatalogueRespDTO> dirList;


}

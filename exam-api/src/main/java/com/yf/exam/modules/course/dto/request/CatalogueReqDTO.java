package com.yf.exam.modules.course.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName CatalogueReqDTO
 * @Description 目录请求类
 * @Author BlueAutumn
 * @Date 2021/8/23 14:25
 * @Version 1.0
 */
@Data
@ApiModel(value = "目录请求", description = "目录请求")
public class CatalogueReqDTO implements Serializable {

    private static final long serialVersionUID = -5105232426690694879L;

    @ApiModelProperty(value = "目录ID", required = true)
    private String catalogueId;
}

package com.yf.exam.modules.catalog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.catalog.dto.CatalogDTO;
import com.yf.exam.modules.catalog.dto.request.CatalogSearchReqDTO;
import com.yf.exam.modules.catalog.entity.Catalog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *@InterfaceName CatalogMapper
 *@Description 分类字典mapper
 *@Author BlueAutumn
 *@Date 2021/9/22 9:09
 *@Version 1.0
 */
public interface CatalogMapper extends BaseMapper<Catalog> {

    IPage<CatalogDTO> paging(Page page, @Param("query") CatalogSearchReqDTO query);

}
package com.yf.exam.modules.course.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.course.dto.request.CoursewareInfoReqDTO;
import com.yf.exam.modules.course.entity.CourseCourseware;

import java.util.List;

/**
 *@InterfaceName CourseCoursewareService
 *@Description 课程课件业务类
 *@Author BlueAutumn
 *@Date 2021/8/23 9:52
 *@Version 1.0
 */
public interface CourseCoursewareService extends IService<CourseCourseware> {
    void saveAll(String courseId, List<CoursewareInfoReqDTO> coursewareInfoReqDTOS);
}
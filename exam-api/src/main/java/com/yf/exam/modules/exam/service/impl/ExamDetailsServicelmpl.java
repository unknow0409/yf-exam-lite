package com.yf.exam.modules.exam.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.exam.dto.ExamDetailsDTO;
import com.yf.exam.modules.exam.dto.request.ExamDetailsReqDTO;
import com.yf.exam.modules.exam.entity.ExamDetails;
import com.yf.exam.modules.exam.mapper.ExamDetailsMapper;
import com.yf.exam.modules.exam.service.ExamDetailsService;
import org.springframework.stereotype.Service;

/**
 * @author lgy
 * @title :ExamDetailsServicelmpl
 * @projectName mylite
 * @Data 2021/8/16 10:30
 */
@Service
public class ExamDetailsServicelmpl extends ServiceImpl<ExamDetailsMapper, ExamDetails> implements ExamDetailsService {

    @Override
    public IPage<ExamDetailsDTO> details(PagingReqDTO<ExamDetailsReqDTO> reqDTO) {
        //创建分页对象
        Page page = new Page(reqDTO.getCurrent(), reqDTO.getSize());

        IPage<ExamDetailsDTO> testData = baseMapper.details(page, reqDTO.getParams());

        return testData;
    }
}

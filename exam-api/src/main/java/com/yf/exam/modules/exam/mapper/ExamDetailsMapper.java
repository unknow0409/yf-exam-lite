package com.yf.exam.modules.exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yf.exam.modules.exam.dto.ExamDetailsDTO;
import com.yf.exam.modules.exam.dto.request.ExamDetailsReqDTO;
import com.yf.exam.modules.exam.entity.ExamDetails;
import org.apache.ibatis.annotations.Param;


/**
 * @author lgy
 * @title :ExamDetailsMapper
 * @projectName mylite
 * @Data 2021/8/16 10:31
 */
public interface ExamDetailsMapper extends BaseMapper<ExamDetails> {

    IPage<ExamDetailsDTO> details(Page page, @Param("query") ExamDetailsReqDTO query);
}

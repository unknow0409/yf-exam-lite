package com.yf.exam.modules.qa.mapper;



import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yf.exam.modules.qa.dto.QaDTO;
import com.yf.exam.modules.qa.entity.Qa;

public interface QaMapper extends BaseMapper<Qa>{
	

	IPage<QaDTO> getQuestionAnswer(Page page,@Param("qaDto") QaDTO qaDto);
}

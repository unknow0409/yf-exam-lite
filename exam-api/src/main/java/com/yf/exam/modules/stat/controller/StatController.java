package com.yf.exam.modules.stat.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yf.exam.core.api.ApiRest;
import com.yf.exam.core.api.controller.BaseController;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.stat.dto.StatUserDTO;
import com.yf.exam.modules.stat.dto.request.StatUserQueryReqDTO;
import com.yf.exam.modules.stat.service.StatUserService;
import com.yf.exam.modules.user.book.dto.UserBookDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 数据控制器
 * </p>
 *
 * @author BlueAutumn
 * @since 2020-08-16 15:11
 */
@Api(tags={"数据"})
@RestController
@RequestMapping("/exam/api/stat/stat")
public class StatController extends BaseController {

    @Autowired
    private StatUserService baseService;

    /**
     * 分页查找
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "分页查找")
    @RequestMapping(value = "/paging", method = { RequestMethod.POST})
    public ApiRest<IPage<StatUserDTO>> paging(@RequestBody PagingReqDTO<StatUserQueryReqDTO> reqDTO) {

        //分页查询并转换
        IPage<StatUserDTO> page = baseService.paging(reqDTO);

        return super.success(page);
    }
}

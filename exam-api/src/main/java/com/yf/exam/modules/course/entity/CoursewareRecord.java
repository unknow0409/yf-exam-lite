package com.yf.exam.modules.course.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;


/**
 * @author lgy
 * @title :CoursewareRecord
 * @projectName mylite
 * @Data 2021/8/27 15:53
 */
@Data
@TableName("el_courseware_record_time")
public class CoursewareRecord extends Model<CoursewareRecord> {

    private static final long serialVersionUID = -8596982501485064176L;

    /**
     *ID
     */
    @TableId(value = "ID", type = IdType.ASSIGN_ID)
    private String id;

    /**
     *用户ID
     */
    @TableField("user_id")
    private String userId;

    /**
     *课程ID
     */
    @TableField("course_id")
    private String courseId;

    /**
     *课件ID
     */
    @TableField("courseware_id")
    private String coursewareId;

    /**
     *
     */
    @TableField("courseware_employ_time")
    private Integer coursewareEmployTime;

    /**
     *创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     *更改时间
     */
    @TableField("change_time")
    private Date changeTime;


}

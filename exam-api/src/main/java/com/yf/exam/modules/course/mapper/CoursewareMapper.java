package com.yf.exam.modules.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.exam.modules.course.dto.request.CoursewareInfoReqDTO;
import com.yf.exam.modules.course.dto.request.CourseUserIDReqDTO;
import com.yf.exam.modules.course.dto.response.CoursewareUseTimeRespDTO;
import com.yf.exam.modules.course.entity.Courseware;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *@InterfaceName CoursewareMapper
 *@Description 课件Mapper
 *@Author BlueAutumn
 *@Date 2021/8/19 15:08
 *@Version 1.0
 */
public interface CoursewareMapper extends BaseMapper<Courseware> {


    List<CoursewareInfoReqDTO> getInfosByCatalogueId(@Param("catalogueId") String catalogueId);

    /**
     * 通过目录ID,课程ID,用户ID查找课件列表
     * @param reqDTO
     * @return
     */
    List<CoursewareUseTimeRespDTO> findCoursewareseTime(CourseUserIDReqDTO reqDTO);

}
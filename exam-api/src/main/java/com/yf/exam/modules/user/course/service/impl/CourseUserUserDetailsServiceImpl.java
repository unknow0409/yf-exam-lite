package com.yf.exam.modules.user.course.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.user.course.dto.CourseDTO;
import com.yf.exam.modules.user.course.dto.request.CourseUserDetailsReqDTO;
import com.yf.exam.modules.user.course.entity.Course;
import com.yf.exam.modules.user.course.mapper.CourseUserDetailsMapper;
import com.yf.exam.modules.user.course.service.CourseUserDetailsService;
import org.springframework.stereotype.Service;

/**
 * @author lgy
 * @title :CourseUserDetailsServiceImpl
 * @projectName mylite
 * @Data 2021/8/19 15:01
 */
@Service
public class CourseUserUserDetailsServiceImpl extends ServiceImpl<CourseUserDetailsMapper, Course> implements CourseUserDetailsService {
    @Override
    public IPage<CourseDTO> courseDetails(PagingReqDTO<CourseUserDetailsReqDTO> reqDTO) {
        //创建分页对象
        Page page = new Page(reqDTO.getCurrent(), reqDTO.getSize());

        IPage<CourseDTO> courseDetails = baseMapper.courseDetails(page, reqDTO.getParams());

        return courseDetails;
    }
}

package com.yf.exam.modules.sys.pdf.controller;

import com.yf.exam.core.api.ApiRest;
import com.yf.exam.core.api.controller.BaseController;
import com.yf.exam.modules.sys.pdf.service.SysPdfService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * <p>
 * Pdf 相关接口
 * </p>
 *
 * @author Mingjun
 * @since 2021-8-11 16:02:01
 */
@Api(tags = {"PDF相关接口"})
@RestController
@RequestMapping("/exam/api/sys/pdf")
public class PdfController extends BaseController {
    @Autowired
    private SysPdfService baseService;

    @ApiOperation(value = "预览PDF")
    @RequestMapping(value = "/preview", method = {RequestMethod.GET})
    @ResponseBody
    public void previewPdf(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String[]> parameterMap = request.getParameterMap();
        if(parameterMap.containsKey("filePath")) {
            baseService.previewPdf(parameterMap.get("filePath")[0], request, response);
        }
    }
}

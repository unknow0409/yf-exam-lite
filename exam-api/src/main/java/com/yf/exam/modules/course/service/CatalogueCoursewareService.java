package com.yf.exam.modules.course.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.course.dto.CatalogueCoursewareDTO;
import com.yf.exam.modules.course.dto.request.CatalogueInfosReqDTO;
import com.yf.exam.modules.course.dto.request.CatalogueReqDTO;
import com.yf.exam.modules.course.dto.request.CoursewareInfoReqDTO;
import com.yf.exam.modules.course.entity.CatalogueCourseware;

import java.util.List;

/**
 *@InterfaceName CatalogueCoursewareService
 *@Description 目录课件业务类
 *@Author BlueAutumn
 *@Date 2021/8/20 20:01
 *@Version 1.0
 */
public interface CatalogueCoursewareService extends IService<CatalogueCourseware> {

    /**
     * 保存所有
     * @param courseId
     * @param catalogueInfoList
     */
    void saveAll(String courseId, List<CatalogueInfosReqDTO> catalogueInfoList);

    /**
     * 根据目录ID查询到响应的课件信息
     * @param reqDTO
     * @return
     */
    List<CatalogueCoursewareDTO> listByCatalogueId(CatalogueReqDTO reqDTO);

}
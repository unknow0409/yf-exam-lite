package com.yf.exam.modules.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yf.exam.modules.course.dto.CoursewareDTO;
import com.yf.exam.modules.course.dto.request.CoursewareDetailsReqDTO;
import com.yf.exam.modules.course.entity.Courseware;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @author lgy
 * @title :CoursewareDetailsMapper
 * @projectName mylite
 * @Data 2021/8/26 14:58
 */
public interface CoursewareDetailsMapper extends BaseMapper<Courseware> {

    IPage<CoursewareDTO> coursewareDetails (Page page, @Param("query") CoursewareDetailsReqDTO query);
}

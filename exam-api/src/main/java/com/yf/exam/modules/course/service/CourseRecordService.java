package com.yf.exam.modules.course.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.course.entity.CourseRecord;

/**
 * @ClassName CourseRecordService
 * @Description 用户学习总课时统计service类
 * @Author Mr.Fu
 * @Date 2021-09-16 14:05:36
 * @Version 1.0
 */
public interface CourseRecordService extends IService<CourseRecord> {

}

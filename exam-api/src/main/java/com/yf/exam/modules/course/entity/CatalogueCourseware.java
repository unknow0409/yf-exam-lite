package com.yf.exam.modules.course.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
 * @ClassName CatalogueCourseware
 * @Description 目录课件表
 * @Author BlueAutumn
 * @Date 2021/8/20 9:05
 * @Version 1.0
 */
@Data
@TableName("el_catalogue_courseware")
public class CatalogueCourseware extends Model<CatalogueCourseware> {

    /**
     * ID
     */
    @TableId(value = "ID", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 目录ID
     */
    @TableField("catalogue_id")
    private String catalogueId;

    /**
     * 课件ID
     */
    @TableField("courseware_id")
    private String coursewareId;

    /**
     * 排序顺序
     */
    private Integer sorting;

    /**
     * 课件学习时长（分钟）
     */
    @TableField("courseware_time")
    private Double coursewareTime;

    /**
     * 课件积分
     */
    @TableField("courseware_integral")
    private Double coursewareIntegral;

    /**
     * 课件类型
     */
    @TableField("courseware_category")
    private String coursewareCategory;

}

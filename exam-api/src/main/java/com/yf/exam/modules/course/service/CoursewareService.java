package com.yf.exam.modules.course.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.course.dto.CoursewareDTO;
import com.yf.exam.modules.course.dto.request.CoursewareInfoReqDTO;
import com.yf.exam.modules.course.dto.request.CourseUserIDReqDTO;
import com.yf.exam.modules.course.dto.response.CoursewareUseTimeRespDTO;
import com.yf.exam.modules.course.entity.Courseware;

import java.util.List;

/**
 *@InterfaceName CoursewareService
 *@Description 课件业务类
 *@Author BlueAutumn
 *@Date 2021/8/19 15:13
 *@Version 1.0
 */
public interface CoursewareService extends IService<Courseware> {

    /**
     * 通过ID查找课件链接
     * @param id
     * @return
     */
    String findAddressById(String id);

    /**
     * 通过目录ID,课程ID,用户ID查找课件列表
     * @param reqDTO
     * @return
     */
    List<CoursewareUseTimeRespDTO> findCoursewareseTime(CourseUserIDReqDTO reqDTO);

    /**
     * 根据课程目录ID获取课程信息
     * @param catalogueId
     * @return
     */
    List<CoursewareInfoReqDTO> getInfoByCatalogueId(String catalogueId);

    /**
     * 添加修改课件
     * @param reqDTO
     * @return
     */
    void saveCourseware(CoursewareDTO reqDTO);

    /**
     * 删除课件
     * @param coursewareIds
     */
    void removeManyById(List<String> coursewareIds);

    /**
     * 根据课件id,获取课件信息
     * @param  coursewareId
     * @return
     *
     */
    CoursewareDTO getInfoById(String coursewareId);
}
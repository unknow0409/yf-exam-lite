package com.yf.exam.modules.exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.exam.dto.ExamDTO;
import com.yf.exam.modules.exam.entity.Exam;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * @author lgy
 * @Data2021/8/10 22:35
 */
public interface ExamSearchService extends IService<Exam> {


    List<String> title(ExamDTO query);
}

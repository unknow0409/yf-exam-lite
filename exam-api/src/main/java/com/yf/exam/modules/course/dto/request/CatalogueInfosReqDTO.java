package com.yf.exam.modules.course.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName CatalogueInfosDTO
 * @Description 目录信息数据传输类
 * @Author BlueAutumn
 * @Date 2021/8/22 11:54
 * @Version 1.0
 */
@Data
@ApiModel(value = "目录信息", description = "目录信息")
public class CatalogueInfosReqDTO implements Serializable {

    private static final long serialVersionUID = -455506750826046881L;

    @ApiModelProperty(value = "课程目录ID", required = true)
    private String id;

    @ApiModelProperty(value = "课程目录名称", required = true)
    private String catalogueName;

    @ApiModelProperty(value = "排序顺序", required = true)
    private Integer sorting;

    @ApiModelProperty(value = "课件信息列表", required = true)
    private List<CoursewareInfoReqDTO> coursewareInfoList;

}

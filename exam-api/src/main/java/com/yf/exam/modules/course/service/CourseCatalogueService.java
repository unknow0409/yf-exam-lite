package com.yf.exam.modules.course.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.course.dto.request.CatalogueInfosReqDTO;
import com.yf.exam.modules.course.dto.request.CourseUserIDReqDTO;
import com.yf.exam.modules.course.dto.response.CourseCatalogueRespDTO;
import com.yf.exam.modules.course.entity.CourseCatalogue;

import java.util.List;

/**
 *@InterfaceName CourseCatalogueService
 *@Description 课程目录业务类
 *@Author BlueAutumn
 *@Date 2021/8/20 14:07
 *@Version 1.0
 */
public interface CourseCatalogueService extends IService<CourseCatalogue> {

    /**
     * 保存所有课程目录以子课件
     * @param courseId
     * @param catalogueInfosReqDTOList
     */
    void saveAll(String courseId, List<CatalogueInfosReqDTO> catalogueInfosReqDTOList);

    /**
     * 根据课程ID查找目录ID列表
     * @param id
     * @return
     */
    List<String> idListByCourseId(String id);

    /**
     * 通过课程ID查找目录的列表
     * @param reqDTO
     * @return
     */
    List<CourseCatalogueRespDTO> findList(CourseUserIDReqDTO reqDTO);

}
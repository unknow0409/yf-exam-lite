package com.yf.exam.modules.qa.dto.request;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

/**
 * @ClassName QaIdsReqDTO
 * @Description 接受问答id列表
 * @Author Mr.Fu
 * @Date 2021-08-26 17:39:45
 * @Version 1.0
 */
@Data
@ApiModel(value = "问答id列表", description = "问答id列表")
public class QaIdsReqDTO {
    List<String> ids;
}

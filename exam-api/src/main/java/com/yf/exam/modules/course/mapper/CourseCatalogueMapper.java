package com.yf.exam.modules.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.exam.modules.course.dto.request.CourseUserIDReqDTO;
import com.yf.exam.modules.course.dto.response.CourseCatalogueRespDTO;
import com.yf.exam.modules.course.entity.CourseCatalogue;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *@InterfaceName CourseCatalogueMapper
 *@Description 课程目录Mapper
 *@Author BlueAutumn
 *@Date 2021/8/20 14:02
 *@Version 1.0
 */

public interface CourseCatalogueMapper extends BaseMapper<CourseCatalogue> {

    List<String> getIdListByCourseId(@Param("courseId") String courseId);

    String getIdByCatalogueNameAndCourseId(@Param("courseId") String courseId, @Param("catalogueName") String catalogueName);

    /**
     * 通过课程ID查找目录的列表
     * @param reqDTO
     * @return
     */
    List<CourseCatalogueRespDTO> findList(CourseUserIDReqDTO reqDTO);

    List<String> idList();
}
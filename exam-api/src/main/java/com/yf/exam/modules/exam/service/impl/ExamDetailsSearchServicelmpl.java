package com.yf.exam.modules.exam.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.modules.exam.dto.request.ExamDetailsReqDTO;
import com.yf.exam.modules.exam.entity.ExamDetails;
import com.yf.exam.modules.exam.mapper.ExamDetailsSearchMapper;
import com.yf.exam.modules.exam.service.ExamDetailsSearchService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lgy
 * @title :ExamDetailsSearchlmpl
 * @projectName mylite
 * @Data 2021/8/16 17:46
 */
@Service
public class ExamDetailsSearchServicelmpl extends ServiceImpl<ExamDetailsSearchMapper, ExamDetails> implements ExamDetailsSearchService {

    @Override
    public List<String> detailsSearch(ExamDetailsReqDTO qurey) {
        return baseMapper.detailsSearch(qurey);
    }

}



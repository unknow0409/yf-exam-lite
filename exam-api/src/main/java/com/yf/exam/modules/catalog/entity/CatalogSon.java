package com.yf.exam.modules.catalog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
 * @ClassName CatalogSon
 * @Description 分类子表
 * @Author Mr.Fu
 * @Date 2021-09-22 08:55:17
 * @Version 1.0
 */
@Data
@TableName("el_catalog_son")
public class CatalogSon extends Model<CatalogSon> {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 分类ID
     */
    @TableField("catalog_id")
    private String catalogId;

    /**
     * 父类ID
     */
    @TableField(value = "parent_id")
    private String parentId;

    /**
     * 分类子表名称
     */
    @TableField("catalog_son_name")
    private String catalogSonName;

    /**
     * 分类子表描述
     */
    @TableField("catalog_son_description")
    private String catalogSonDescription;

    /**
     * 分类子表编码
     */
    @TableField("catalog_son_code")
    private String catalogSonCode;

}

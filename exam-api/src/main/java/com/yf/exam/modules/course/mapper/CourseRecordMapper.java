package com.yf.exam.modules.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.exam.modules.course.entity.CourseRecord;

/**
 * @ClassName CourseRecordMapper
 * @Description 用户学习总课时统计
 * @Author Mr.Fu
 * @Date 2021-09-16 14:37:20
 * @Version 1.0
 */
public interface CourseRecordMapper extends BaseMapper<CourseRecord> {

}

package com.yf.exam.modules.catalog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName Catalog
 * @Description 分类字典实体类
 * @Author BlueAutumn
 * @Date 2021/9/22 8:57
 * @Version 1.0
 */
@Data
@TableName("el_catalog")
public class Catalog extends Model<Catalog> {

    private static final long serialVersionUID = -9137555070744009190L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 分类字典名称
     */
    @TableField("catalog_name")
    private String catalogName;

    /**
     * 分类字典编码
     */
    @TableField("catalog_code")
    private String catalogCode;

    /**
     * 分类字典描述
     */
    private String description;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;
}

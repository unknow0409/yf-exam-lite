package com.yf.exam.modules.course.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.course.dto.request.CourseUserIDReqDTO;
import com.yf.exam.modules.course.dto.response.CoursewareVideoInfoRespDTO;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.course.dto.CourseDTO;
import com.yf.exam.modules.course.dto.request.CoursePagReqDTO;
import com.yf.exam.modules.course.dto.request.CourseSaveReqDTO;
import com.yf.exam.modules.course.dto.response.CourseInfoRespDTO;
import com.yf.exam.modules.course.entity.Course;

import java.util.List;

/**
 * @ClassName CourseService
 * @Description 课程业务类
 * @Author BlueAutumn
 * @Date 2021/8/19 10:57
 * @Version 1.0
 */
public interface CourseService extends IService<Course> {

    /**
     * 通过课程ID和用户ID获取课程的详细信息及目录列表
     * @param reqDTO
     * @return
     */
    CoursewareVideoInfoRespDTO findCourseAndCatalogue(CourseUserIDReqDTO reqDTO);

    /**
     * 分页查询及筛选
     * @param reqDTO
     * @return
     */
    IPage<CourseDTO> paging(PagingReqDTO<CoursePagReqDTO> reqDTO);

    /**
     * 保存课程信息
     * @param reqDTO
     */
    void save(CourseSaveReqDTO reqDTO);

    /**
     * 根据ID获取课程信息
     * @param courseId
     * @return
     */
    CourseInfoRespDTO getInfoById(String courseId);

    /**
     * 删除与该课程ID相关联的信息
     * @param courseIds
     */
    void removeBatchByIds(List<String> courseIds);
}

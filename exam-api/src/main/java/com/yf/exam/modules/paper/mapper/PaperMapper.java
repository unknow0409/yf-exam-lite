package com.yf.exam.modules.paper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yf.exam.modules.paper.dto.PaperDTO;
import com.yf.exam.modules.paper.dto.request.PaperListReqDTO;
import com.yf.exam.modules.paper.dto.response.PaperListRespDTO;
import com.yf.exam.modules.paper.entity.Paper;
import org.apache.ibatis.annotations.Param;

import java.sql.Timestamp;
import java.util.List;

/**
* <p>
* 试卷Mapper
* </p>
*
* @author 聪明笨狗
* @since 2020-05-25 16:33
*/
public interface PaperMapper extends BaseMapper<Paper> {

    /**
     * 查找试卷分页
     * @param page
     * @param query
     * @return
     */
    IPage<PaperListRespDTO> paging(Page page, @Param("query") PaperListReqDTO query);

    /**
     * 查找试卷状态
     * @param userId
     * @param examId
     * @return
     */
    List<String> findPaperState(@Param("userId") String userId,@Param("examId") String examId);

    /**
     * 试卷列表响应类
     * @param query
     * @return
     */
    List<PaperListRespDTO> list(@Param("query") PaperDTO query);

    /**
     * 修改试卷题目批改状态
     * @param paperId
     * @param quId
     * @param score
     * @return
     */
    int updatePaperScore(@Param("paperId") String paperId,@Param("quId") String quId,@Param("score") int score);

    /**
     * 修改用户成绩及试卷状态
     * @param paperId
     * @return
     */
    int updateUserScore(@Param("paperId") String paperId);

    /**
     * 修改过期未交卷试卷状态
     * @return
     */
    int updatePaperState(@Param("date")Timestamp date);
}

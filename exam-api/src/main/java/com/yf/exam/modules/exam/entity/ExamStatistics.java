package com.yf.exam.modules.exam.entity;

/**
 * @author lgy
 * @Data2021/8/1216:31
 */

public class ExamStatistics {
    /**
     *科目
     */
    private String subject;

    /**
     *考试时间
     */

    private String date;

    /**
     *考试人次
     */

    private String examNum;

    /**
     *考生人数
     */

    private  String numofPeople;

    /**
     *通过人数
     */

    private String passNum;

    /**
     *通过率
     */

    private  String passPro;

}

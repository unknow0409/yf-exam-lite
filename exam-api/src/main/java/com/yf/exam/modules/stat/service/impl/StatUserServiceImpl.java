package com.yf.exam.modules.stat.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.stat.dto.StatUserDTO;
import com.yf.exam.modules.stat.dto.request.StatUserQueryReqDTO;
import com.yf.exam.modules.stat.entity.StatUser;
import com.yf.exam.modules.stat.mapper.StatUserMapper;
import com.yf.exam.modules.stat.service.StatUserService;
import org.springframework.stereotype.Service;

/**
 * @ClassName StatUserServiceImpl
 * @Description 用户统计业务实现类
 * @Author BlueAutumn
 * @Date 2021/8/16 16:51
 * @Version 1.0
 */
@Service
public class StatUserServiceImpl extends ServiceImpl<StatUserMapper, StatUser> implements StatUserService {

    @Override
    public IPage<StatUserDTO> paging(PagingReqDTO<StatUserQueryReqDTO> reqDTO) {

        //创建分页对象
        Page page = new Page<>(reqDTO.getCurrent(), reqDTO.getSize());

        //转换结果
        IPage<StatUserDTO> pageData = baseMapper.paging(page, reqDTO.getParams());
        return pageData;
    }

}

package com.yf.exam.modules.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.exam.modules.course.dto.CourseDTO;
import com.yf.exam.modules.course.entity.Course;

import java.util.List;


/**
 * @author lgy
 * @title :CourseSearchMapper
 * @projectName mylite
 * @Data 2021/8/20 8:56
 */
public interface CourseSearchMapper extends BaseMapper<Course> {

    List<String> courseSearch(CourseDTO query);
}

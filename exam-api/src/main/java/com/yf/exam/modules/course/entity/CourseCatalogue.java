package com.yf.exam.modules.course.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
 * @ClassName CourseCatalogue
 * @Description 课程目录实体类
 * @Author BlueAutumn
 * @Date 2021/8/20 8:44
 * @Version 1.0
 */
@Data
@TableName("el_course_catalogue")
public class CourseCatalogue extends Model<CourseCatalogue> {

    private static final long serialVersionUID = -308149071877139786L;

    /**
     * 课程目录ID
     */
    @TableId(value = "ID", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 课程目录名称
     */
    @TableField("catalogue_name")
    private String catalogueName;

    /**
     * 课程ID
     */
    @TableField("course_id")
    private String courseId;

    /**
     * 排序顺序
     */
    private Integer sorting;
}

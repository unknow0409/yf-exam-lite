package com.yf.exam.modules.qa.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.course.entity.CatalogueCourseware;
import com.yf.exam.modules.qa.dto.QaDTO;
import com.yf.exam.modules.qa.entity.Qa;


public interface QaService extends IService<Qa> {
	
	IPage<QaDTO> getQuestionAnswer(PagingReqDTO<QaDTO> qaDTO);

	void save(QaDTO qaDTO);

	void updata(QaDTO qaDTO);

}

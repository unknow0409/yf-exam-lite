package com.yf.exam.modules.course.dto.response;

import com.yf.exam.modules.course.dto.request.CatalogueInfosReqDTO;
import com.yf.exam.modules.course.entity.Course;
import com.yf.exam.modules.sys.depart.entity.SysDepart;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName CourseRespDTO
 * @Description 课程信息响应类
 * @Author BlueAutumn
 * @Date 2021/8/24 8:31
 * @Version 1.0
 */
@Data
@ApiModel(value = "课程信息", description = "课程信息")
public class CourseInfoRespDTO extends Course {

    private static final long serialVersionUID = -7753113168188910210L;

    @ApiModelProperty(value = "部门信息列表", required = true)
    private List<SysDepart> departList;

    @ApiModelProperty(value = "课程目录信息表", required = true)
    private List<CatalogueInfosReqDTO> catalogueInfoList;

}

package com.yf.exam.modules.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.exam.modules.course.entity.CoursewareCategory;

/**
 *@InterfaceName CoursewareCategoryMapper
 *@Description 课件类型Mapper
 *@Author BlueAutumn
 *@Date 2021/8/26 17:39
 *@Version 1.0
 */
public interface CoursewareCategoryMapper extends BaseMapper<CoursewareCategory> {
}
package com.yf.exam.modules.qa.controller;

import com.yf.exam.core.utils.UploadUtils;
import com.yf.exam.modules.qa.dto.request.QaIdsReqDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yf.exam.core.api.ApiRest;
import com.yf.exam.core.api.controller.BaseController;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.qa.dto.QaDTO;
import com.yf.exam.modules.qa.service.QaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Api(tags={"问答"})
@RestController
@RequestMapping("/exam/api/qa")
public class QaController extends BaseController{

	@Value("${web.upload-path}")
	private String fillePath;
	
	@Autowired
	private QaService qaService;
	
	@ApiOperation(value = "问题列表接口")
    @RequestMapping(value = "/detail", method = { RequestMethod.POST})
    public ApiRest<IPage<QaDTO>> getQuestionAnswer(@RequestBody PagingReqDTO<QaDTO> qaDTO) {
		IPage<QaDTO> questionAnswer = qaService.getQuestionAnswer(qaDTO);
        return super.success(questionAnswer);
    }
	@ApiOperation(value = "提问保存")
    @RequestMapping(value = "/save", method = { RequestMethod.POST})
	public ApiRest save(@RequestBody QaDTO qaDTO) {
		qaService.save(qaDTO);
		return super.success();
	}
	@ApiOperation(value = "提问答复保存")
    @RequestMapping(value = "/updata", method = { RequestMethod.POST})
	public ApiRest updata(@RequestBody QaDTO qaDTO) {
		qaService.updata(qaDTO);
		return super.success();
	}

	@ApiOperation(value = "批量删除")
	@RequestMapping(value = "/delete", method = { RequestMethod.POST})
	public ApiRest delete(@RequestBody QaIdsReqDTO reqDTO){
		qaService.removeByIds(reqDTO.getIds());
		return super.success();
	}
	@ApiOperation(value = "图片的保存")
	@RequestMapping(value = "/img", method = { RequestMethod.POST})
	public ApiRest imguUpload(@RequestBody MultipartFile file){
		//上传的文件，并返回路径
		String path= UploadUtils.saveFile(file, "Qa/img", fillePath);
		return super.success(path);
	}

	
	
}

package com.yf.exam.modules.course.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName CoursePagReqDTO
 * @Description 课程分页查询请求类
 * @Author BlueAutumn
 * @Date 2021/8/19 16:19
 * @Version 1.0
 */
@Data
@ApiModel(value = "课程分页查询请求类", description = "课程分页查询请求类")
public class CoursePagReqDTO implements Serializable {

    private static final long serialVersionUID = -6683715044678811845L;

    @ApiModelProperty(value = "课程是否必修，0非必修,1必修", required = true)
    private Integer isNecessary;

    @ApiModelProperty(value = "课程分类", required = true)
    private String courseCategory;

    @ApiModelProperty(value = "课程名称", required = true)
    private String courseName;
}

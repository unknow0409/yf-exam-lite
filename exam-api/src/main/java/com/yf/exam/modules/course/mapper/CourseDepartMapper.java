package com.yf.exam.modules.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.exam.modules.course.entity.CourseDepart;

/**
 *@InterfaceName CourseDepartMapper
 *@Description 课程部门Mapper
 *@Author BlueAutumn
 *@Date 2021/8/20 9:53
 *@Version 1.0
 */
public interface CourseDepartMapper extends BaseMapper<CourseDepart> {

}
package com.yf.exam.modules.course.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
 * @ClassName CourseDepart
 * @Description 课程部门表
 * @Author BlueAutumn
 * @Date 2021/8/20 9:36
 * @Version 1.0
 */
@Data
@TableName("el_course_depart")
public class CourseDepart extends Model<CourseDepart> {

    private static final long serialVersionUID = 69409866218373529L;

    /**
     * ID
     */
    private String id;

    /**
     * 课程ID
     */
    @TableField("course_id")
    private String courseId;

    /**
     * 部门ID
     */
    @TableField("depart_id")
    private String departId;

}

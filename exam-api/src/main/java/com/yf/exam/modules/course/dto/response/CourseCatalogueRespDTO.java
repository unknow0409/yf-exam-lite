package com.yf.exam.modules.course.dto.response;

import com.yf.exam.modules.course.dto.CourseCatalogueDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName CourseCatalogueRespDTO
 * @Description 目录的顺序响应类
 * @Author Mr.Fu
 * @Date 2021-08-20 14:38:03
 * @Version 1.0
 */
@Data
public class CourseCatalogueRespDTO extends CourseCatalogueDTO {

    @ApiModelProperty(value = "课件的详细信息", required = true)
    private List<CoursewareUseTimeRespDTO> fileList;

}

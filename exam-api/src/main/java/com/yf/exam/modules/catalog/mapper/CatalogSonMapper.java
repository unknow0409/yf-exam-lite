package com.yf.exam.modules.catalog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.exam.modules.catalog.dto.request.CatalogSonDataReqDTO;
import com.yf.exam.modules.catalog.entity.CatalogSon;
import com.yf.exam.modules.course.dto.response.CourseCategoryTreeRespDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ClassName CatalogSonMapper
 * @Description 分类子表
 * @Author Mr.Fu
 * @Date 2021-09-22 09:00:44
 * @Version 1.0
 */
public interface CatalogSonMapper extends BaseMapper<CatalogSon> {
    void deleteByCatalogId(@Param("catalogId") String catalogId);

    List<CourseCategoryTreeRespDTO> findFather(@Param("code") String code);

    List<CourseCategoryTreeRespDTO> findChildren(@Param("parentId") String parentId);

    List<CatalogSon> findAllCatalogSon(CatalogSonDataReqDTO reqDTO);
}

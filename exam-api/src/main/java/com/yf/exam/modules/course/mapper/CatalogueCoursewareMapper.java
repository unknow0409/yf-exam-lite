package com.yf.exam.modules.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.exam.modules.course.dto.CatalogueCoursewareDTO;
import com.yf.exam.modules.course.entity.CatalogueCourseware;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *@InterfaceName CatalogueCoursewareMapper
 *@Description 目录课件Mapper
 *@Author BlueAutumn
 *@Date 2021/8/20 14:02
 *@Version 1.0
 */
public interface CatalogueCoursewareMapper extends BaseMapper<CatalogueCourseware> {

    List<CatalogueCoursewareDTO> getListByCatalogueId(@Param("catalogueId") String catalogueId);

    void removeUselessByCatalogueIds(@Param("catalogueIdList") List<String> catalogueIdList);
}
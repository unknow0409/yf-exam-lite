package com.yf.exam.modules.course.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName CourseUserIDReqDTO
 * @Description 获取用户ID和课程ID
 * @Author Mr.Fu
 * @Date 2021-08-20 14:56:59
 * @Version 1.0
 */
@Data
public class CourseUserIDReqDTO {

    @ApiModelProperty(value = "用户ID", required = true)
    private String userID;

    @ApiModelProperty(value = "课程ID", required = true)
    private String courseID;

    @ApiModelProperty(value = "目录ID", required = true)
    private String catalogueID;


}

package com.yf.exam.modules.course.dto.response;

import com.yf.exam.modules.course.dto.request.CoursewareInfoReqDTO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @ClassName CoursewareInfoRespDTO
 * @Description 课程课件信息响应类
 * @Author BlueAutumn
 * @Date 2021/8/24 9:26
 * @Version 1.0
 */
@Data
@ApiModel(value = "课程课件信息", description = "课程课件信息")
public class CoursewareInfoRespDTO extends CoursewareInfoReqDTO {
}

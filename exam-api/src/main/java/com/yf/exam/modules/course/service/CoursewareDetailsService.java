package com.yf.exam.modules.course.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.course.dto.CoursewareDTO;
import com.yf.exam.modules.course.dto.request.CoursewareDetailsReqDTO;
import com.yf.exam.modules.course.entity.Courseware;

/**
 * @author lgy
 * @title :CoursewareDetailsService
 * @projectName mylite
 * @Data 2021/8/26 14:59
 */
public interface CoursewareDetailsService  extends IService<Courseware> {

    IPage<CoursewareDTO> coursewareDetails(PagingReqDTO<CoursewareDetailsReqDTO> reqDTO);
}

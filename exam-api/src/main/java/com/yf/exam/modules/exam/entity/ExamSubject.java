package com.yf.exam.modules.exam.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 考试题目实体类
 * </p>
 */
@Data
public class ExamSubject {

    /**
     * 试卷id
     */
    private String paperId;

    /**
     * 问题id
     */
    private String quId;

    /**
     * 题目类型
     */
    private int quType;

    /**
     * 实际成绩
     */
    private int actualScore;

    /**
     * 难度
     */
    private int level;

    /**
     * 题目
     */
    private String content;

    /**
     * 是否批改
     */
    private int isReview;

    /**
     * 是否正确
     */
    private int isRight;
}

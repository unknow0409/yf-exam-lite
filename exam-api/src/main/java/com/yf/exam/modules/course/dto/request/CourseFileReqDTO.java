package com.yf.exam.modules.course.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

/**
 * @ClassName CourseFileReqDTO
 * @Description 文件信息请求类
 * @Author Mr.Fu
 * @Date 2021-08-27 14:18:26
 * @Version 1.0
 */
@Data
@ApiModel(value = "文件信息", description = "文件信息")
public class CourseFileReqDTO implements Serializable {

    @ApiModelProperty(value = "目录的路径", required = true)
    private String path;

}

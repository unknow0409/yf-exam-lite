package com.yf.exam.modules.stat.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName StatUserQueryDTO
 * @Description 用户统计查询请求类
 * @Author BlueAutumn
 * @Date 2021/8/17 15:05
 * @Version 1.0
 */
@Data
@ApiModel(value="用户统计查询请求类", description="用户统计查询请求类")
public class StatUserQueryReqDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "开始时间")
    private Date startTime;

    @ApiModelProperty(value = "结束时间")
    private Date endTime;
}

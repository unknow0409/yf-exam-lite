package com.yf.exam.modules.exam.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "用户试卷题目答案",description = "用户试卷题目答案")
public class ExamAnswerDTO {

    @ApiModelProperty(value = "试卷id")
    private String paperId;

    @ApiModelProperty(value = "问题题目id")
    private String quId;

    @ApiModelProperty(value = "单题总分")
    private int score;

    @ApiModelProperty(value = "实际得分")
    private int actualScore;

    @ApiModelProperty(value = "是否正确")
    private int isRight;

    @ApiModelProperty(value = "用户解析")
    private String examAnalysis;

    @ApiModelProperty(value = "正确解析")
    private String rightAnalysis;

    @ApiModelProperty(value = "用户选项")
    private String examAbc;

    @ApiModelProperty(value = "正确选项")
    private String rightAbc;
}

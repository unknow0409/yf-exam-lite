package com.yf.exam.modules.catalog.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yf.exam.core.api.ApiRest;
import com.yf.exam.core.api.controller.BaseController;
import com.yf.exam.core.api.dto.BaseIdsReqDTO;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.catalog.dto.CatalogDTO;
import com.yf.exam.modules.catalog.dto.request.CatalogSearchReqDTO;
import com.yf.exam.modules.catalog.service.CatalogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName CatalogController
 * @Description 分类字典控制类
 * @Author BlueAutumn
 * @Date 2021/9/22 9:21
 * @Version 1.0
 */
@Api
@RestController
@RequestMapping("/catalog/api/catalog/catalog")
public class CatalogController extends BaseController {

    @Autowired
    private CatalogService baseService;

    /**
     * 分类字典查询
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "分类字典查询")
    @RequestMapping(value = "/paging", method = {RequestMethod.POST})
    public ApiRest<IPage<CatalogDTO>> paging(@RequestBody PagingReqDTO<CatalogSearchReqDTO> reqDTO){

        IPage<CatalogDTO> searchData = baseService.paging(reqDTO);

        return super.success(searchData);
    }

    /**
     * 分类字典增加/修改接口
     * @param dto
     * @return
     */
    @ApiOperation(value = "分类字典增加/修改接口")
    @RequestMapping(value = "/save", method = {RequestMethod.POST})
    public ApiRest save(@RequestBody CatalogDTO dto){

        baseService.save(dto);

        return super.success();
    }

    /**
     * 分类字典的批量删除
     * @param ids
     * @return
     */
    @ApiOperation(value = "分类字典的批量删除")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public ApiRest delete(@RequestBody BaseIdsReqDTO reqDTO){

        baseService.deleteByIds(reqDTO.getIds());

        return super.success();
    }

}

package com.yf.exam.modules.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.core.exception.ServiceException;
import com.yf.exam.modules.course.dto.request.CatalogueInfosReqDTO;
import com.yf.exam.modules.course.dto.request.CourseUserIDReqDTO;
import com.yf.exam.modules.course.dto.response.CourseCatalogueRespDTO;
import com.yf.exam.modules.course.entity.CatalogueCourseware;
import com.yf.exam.modules.course.entity.CourseCatalogue;
import com.yf.exam.modules.course.mapper.CourseCatalogueMapper;
import com.yf.exam.modules.course.service.CatalogueCoursewareService;
import com.yf.exam.modules.course.service.CourseCatalogueService;
import com.yf.exam.modules.course.service.CourseCoursewareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName CourseCatalogueServiceImpl
 * @Description 课程目录业务实现类
 * @Author BlueAutumn
 * @Date 2021/8/20 14:08
 * @Version 1.0
 */
@Service
public class CourseCatalogueServiceImpl extends ServiceImpl<CourseCatalogueMapper, CourseCatalogue> implements CourseCatalogueService {

    @Autowired
    public CourseCatalogueMapper baseMapper;

    @Override
    public void saveAll(String courseId, List<CatalogueInfosReqDTO> catalogueInfos) {

        // 先删除原有的目录
        QueryWrapper<CourseCatalogue> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(CourseCatalogue::getCourseId, courseId);
        this.remove(wrapper);

        // 再增加传递过来的所有目录
        if (CollectionUtils.isEmpty(catalogueInfos)) {
            throw new ServiceException(1, "请至少增加一个目录");
        }
        List<CourseCatalogue> list = new ArrayList<>();

        for (CatalogueInfosReqDTO catalogueInfo : catalogueInfos) {
            CourseCatalogue catalogue = new CourseCatalogue();
            catalogue.setCatalogueName(catalogueInfo.getCatalogueName());
            catalogue.setSorting(catalogueInfo.getSorting());
            catalogue.setCourseId(courseId);
            list.add(catalogue);
        }
        this.saveBatch(list);
    }

    @Override
    public List<String> idListByCourseId(String courseId) {

        return baseMapper.getIdListByCourseId(courseId);
    }

    /**
     * 通过课程ID查找目录的列表
     * @param reqDTO
     * @return
     */
    @Override
    public List<CourseCatalogueRespDTO> findList(CourseUserIDReqDTO reqDTO) {
        return baseMapper.findList(reqDTO);
    }

}

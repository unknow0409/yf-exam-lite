package com.yf.exam.modules.user.course.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.modules.user.course.dto.CourseDTO;
import com.yf.exam.modules.user.course.entity.Course;
import com.yf.exam.modules.user.course.mapper.CourseUserCategorySearchMapper;
import com.yf.exam.modules.user.course.service.CourseUserCategorySearchService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lgy
 * @title :CourseUserCategorySearchServiceImpl
 * @projectName mylite
 * @Data 2021/9/14 9:52
 */
@Service
public class CourseUserCategorySearchServiceImpl extends ServiceImpl<CourseUserCategorySearchMapper, Course> implements CourseUserCategorySearchService {


    @Override
    public List<String> courseUsercategorySearch(CourseDTO query) {
        return baseMapper.courseUserCategorySearch(query);
    }
}

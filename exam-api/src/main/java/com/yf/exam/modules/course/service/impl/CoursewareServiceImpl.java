package com.yf.exam.modules.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.core.utils.BeanMapper;
import com.yf.exam.core.utils.StringUtils;
import com.yf.exam.modules.course.dto.CoursewareDTO;
import com.yf.exam.modules.course.dto.request.CourseUserIDReqDTO;
import com.yf.exam.modules.course.dto.request.CoursewareInfoReqDTO;
import com.yf.exam.modules.course.dto.response.CoursewareUseTimeRespDTO;
import com.yf.exam.modules.course.entity.*;
import com.yf.exam.modules.course.mapper.CoursewareMapper;
import com.yf.exam.modules.course.service.CatalogueCoursewareService;
import com.yf.exam.modules.course.service.CourseCoursewareService;
import com.yf.exam.modules.course.service.CoursewareRecordService;
import com.yf.exam.modules.course.service.CoursewareService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ClassName CoursewareServiceImpl
 * @Description 课件业务实现类
 * @Author BlueAutumn
 * @Date 2021/8/19 15:14
 * @Version 1.0
 */
@Service
public class CoursewareServiceImpl extends ServiceImpl<CoursewareMapper, Courseware> implements CoursewareService {

    @Autowired
    private  CoursewareService coursewareService;

    @Autowired
    private CourseCoursewareService courseCoursewareService;

    @Autowired
    private CatalogueCoursewareService catalogueCoursewareService;

    @Autowired
    private CoursewareRecordService coursewareRecordService;




    /**
     * 通过ID获取课件链接
     * @param id
     * @return
     */
    @Override
    public String findAddressById(String id) {
        QueryWrapper<Courseware> wrapper=new QueryWrapper<>();
        wrapper.eq("id",id).select("address");
        Courseware courseware = baseMapper.selectOne(wrapper);
        return courseware.getAddress();
    }

    /**
     * 通过目录ID,课程ID,用户ID查找课件列表
     * @param reqDTO
     * @return
     */
    @Override
    public List<CoursewareUseTimeRespDTO> findCoursewareseTime(CourseUserIDReqDTO reqDTO) {
        //1.查找出该课程下的所有课件
        QueryWrapper<CourseCourseware> wrapperCourseCourseware=new QueryWrapper<>();
        wrapperCourseCourseware.lambda().eq(CourseCourseware::getCourseId,reqDTO.getCourseID());
        List<CourseCourseware> courseCoursewares = courseCoursewareService.list(wrapperCourseCourseware);
        //2.通过课程、课件和用户ID查找是否有课件记录
        for (CourseCourseware courseCourseware : courseCoursewares) {
            QueryWrapper<CoursewareRecord> wrapperCoursewareRecord=new QueryWrapper<>();
            wrapperCoursewareRecord.lambda().eq(CoursewareRecord::getCourseId,reqDTO.getCourseID())
                    .eq(CoursewareRecord::getCoursewareId,courseCourseware.getCoursewareId())
                    .eq(CoursewareRecord::getUserId,reqDTO.getUserID());
            //3.如果没有记录，则添加记录
            if (coursewareRecordService.getOne(wrapperCoursewareRecord)==null){
                CoursewareRecord coursewareRecord=new CoursewareRecord();
                coursewareRecord.setCourseId(reqDTO.getCourseID());
                coursewareRecord.setUserId(reqDTO.getUserID());
                coursewareRecord.setCoursewareId(courseCourseware.getCoursewareId());
                coursewareRecord.insert();
            }
        }
        return baseMapper.findCoursewareseTime(reqDTO);
    }

    @Override
    public List<CoursewareInfoReqDTO> getInfoByCatalogueId(String catalogueId) {
        return baseMapper.getInfosByCatalogueId(catalogueId);
    }

    /**
     * 根据课件id,获取课件信息
     * @param courserwareId
     * @return
     *
     */
    @Override
    public CoursewareDTO getInfoById(String courserwareId) {

        CoursewareDTO dto = new CoursewareDTO();

        // 获取课件信息并复制
        Courseware courseware = baseMapper.selectById(courserwareId);
        BeanUtils.copyProperties(courseware, dto);

        return dto;
    }



    /**
     * 添加或修改课件
     * @param reqDTO
     */
    @Override
    public void  saveCourseware(CoursewareDTO reqDTO){

       //ID
       String id = reqDTO.getId();

       //复制参数
        Courseware entity = new Courseware();

        BeanMapper.copy(reqDTO, entity);
        //判断id是否为空
        if (StringUtils.isBlank(id)) {

            id = IdWorker.getIdStr();
            entity.setId(id);

        } else {
            //获取修改时间
            entity.setChangeTime(new Date(new Date().getTime()));
        }

       this.saveOrUpdate(entity);

    }

    /**
     * 删除课件
     * @param coursewareIds
     */
    @Override
    public void removeManyById (List<String> coursewareIds){

        //删除课程与课件的对应联系
        for(String coursewareId : coursewareIds ){
            QueryWrapper<CourseCourseware> courseCoursewareQueryWrapper = new QueryWrapper<>();
            courseCoursewareQueryWrapper.lambda().eq(CourseCourseware::getCoursewareId,coursewareId);
            courseCoursewareService.remove(courseCoursewareQueryWrapper);

        }

        //删除课程目录与课件的记录
        for(String coursewareId : coursewareIds){
                QueryWrapper<CatalogueCourseware> catalogueCoursewareQueryWrapper = new QueryWrapper<>();
                catalogueCoursewareQueryWrapper.lambda().eq(CatalogueCourseware::getCoursewareId,coursewareId);
                catalogueCoursewareService.remove(catalogueCoursewareQueryWrapper);
        }
        //删除用户记录表记录
        for(String coursewareId : coursewareIds){
                QueryWrapper<CoursewareRecord>coursewareRecordQueryWrapper =new QueryWrapper<>();
                coursewareRecordQueryWrapper.lambda().eq(CoursewareRecord::getCoursewareId,coursewareId);
                coursewareRecordService.remove(coursewareRecordQueryWrapper);
        }

        // 删除课件
         this.removeByIds(coursewareIds);


    }

}

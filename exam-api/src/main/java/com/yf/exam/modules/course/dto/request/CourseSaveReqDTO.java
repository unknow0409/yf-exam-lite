package com.yf.exam.modules.course.dto.request;

import com.yf.exam.modules.course.entity.Course;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName CourseSaveReqDTO
 * @Description 课程保存请求类
 * @Author BlueAutumn
 * @Date 2021/8/19 15:49
 * @Version 1.0
 */
@Data
@ApiModel(value = "课程保存请求类", description = "课程保存请求类")
public class CourseSaveReqDTO extends Course {

    private static final long serialVersionUID = -1946914047054647145L;

    @ApiModelProperty(value = "课程部门列表", required = true)
    private List<String> departIds;

    @ApiModelProperty(value = "目录信息列表", required = true)
    private List<CatalogueInfosReqDTO> catalogueInfoList;

}

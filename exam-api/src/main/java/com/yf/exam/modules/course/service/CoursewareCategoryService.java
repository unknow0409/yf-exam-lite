package com.yf.exam.modules.course.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.course.dto.response.CoursewareCategoryRespDTO;
import com.yf.exam.modules.course.entity.CoursewareCategory;

/**
 *@InterfaceName CoursewareCategoryService
 *@Description 课件类型业务类
 *@Author BlueAutumn
 *@Date 2021/8/26 17:40
 *@Version 1.0
 */
public interface CoursewareCategoryService extends IService<CoursewareCategory> {

    /**
     * 课件类型名字列表
     * @return
     */
    CoursewareCategoryRespDTO listName();
}
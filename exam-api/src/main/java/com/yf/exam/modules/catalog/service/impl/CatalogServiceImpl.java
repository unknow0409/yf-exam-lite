package com.yf.exam.modules.catalog.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.core.utils.BeanMapper;
import com.yf.exam.core.utils.StringUtils;
import com.yf.exam.modules.catalog.dto.CatalogDTO;
import com.yf.exam.modules.catalog.dto.request.CatalogSearchReqDTO;
import com.yf.exam.modules.catalog.entity.Catalog;
import com.yf.exam.modules.catalog.mapper.CatalogMapper;
import com.yf.exam.modules.catalog.mapper.CatalogSonMapper;
import com.yf.exam.modules.catalog.service.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName CatalogServiceImpl
 * @Description 分类字典业务实现类
 * @Author BlueAutumn
 * @Date 2021/9/22 9:23
 * @Version 1.0
 */
@Service
public class CatalogServiceImpl extends ServiceImpl<CatalogMapper, Catalog> implements CatalogService {

    @Autowired
    private CatalogMapper baseMapper;

    @Autowired
    private CatalogSonMapper catalogSonMapper;

    @Override
    public IPage<CatalogDTO> paging(PagingReqDTO<CatalogSearchReqDTO> reqDTO) {

        Page page = new Page(reqDTO.getCurrent(), reqDTO.getSize());

        IPage<CatalogDTO> detailData = baseMapper.paging(page, reqDTO.getParams());

        return detailData;
    }

    @Override
    public void save(CatalogDTO dto) {

        // ID
        String id = dto.getId();

        if (StringUtils.isBlank(id)) {
            id = IdWorker.getIdStr();
        }

        // 复制参数
        Catalog catalog = new Catalog();

        BeanMapper.copy(dto, catalog);

        this.saveOrUpdate(catalog);
    }

    @Override
    public void deleteByIds(List<String> ids) {

        for (String id : ids) {
            // 先删儿子后删爹
            catalogSonMapper.deleteByCatalogId(id);
            baseMapper.deleteById(id);
        }
    }
}

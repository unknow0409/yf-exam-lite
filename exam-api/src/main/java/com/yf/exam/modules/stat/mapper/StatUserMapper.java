package com.yf.exam.modules.stat.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yf.exam.modules.stat.dto.StatUserDTO;
import com.yf.exam.modules.stat.dto.request.StatUserQueryReqDTO;
import com.yf.exam.modules.stat.entity.StatUser;
import org.apache.ibatis.annotations.Param;

/**
 *@InterfaceName StatUserMapper
 *@Description 用户统计Mapper
 *@Author BlueAutumn
 *@Date 2021/8/16 16:45
 *@Version 1.0
 */
public interface StatUserMapper extends BaseMapper<StatUser> {

    IPage<StatUserDTO> paging(Page page, @Param("query") StatUserQueryReqDTO query);
}
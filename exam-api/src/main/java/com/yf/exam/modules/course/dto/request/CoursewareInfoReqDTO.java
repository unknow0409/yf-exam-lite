package com.yf.exam.modules.course.dto.request;

import com.yf.exam.modules.course.dto.CoursewareDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName CoursewareInfoDTO
 * @Description 课件信息数据请求类
 * @Author BlueAutumn
 * @Date 2021/8/20 15:41
 * @Version 1.0
 */
@Data
@ApiModel(value = "课件信息数据请求类", description = "课件信息数据请求类")
public class CoursewareInfoReqDTO extends CoursewareDTO{

    private static final long serialVersionUID = 1153647231731455913L;

    @ApiModelProperty(value = "课件ID", required = true)
    private String coursewareId;

    @ApiModelProperty(value = "课程目录名称", required = true)
    private String catalogueName;

    @ApiModelProperty(value = "课件学习时长（分钟）", required = true)
    private String coursewareTime;

    @ApiModelProperty(value = "课件学习积分", required = true)
    private String coursewareIntegral;

    @ApiModelProperty(value = "排序顺序", required = true)
    private Integer sorting;

}

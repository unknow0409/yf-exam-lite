package com.yf.exam.modules.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yf.exam.modules.course.dto.CourseDetailsDTO;
import com.yf.exam.modules.course.dto.request.CourseDetailsReqDTO;
import com.yf.exam.modules.course.entity.CourseDetails;
import org.apache.ibatis.annotations.Param;

/**
 * @author lgy
 * @title :CourseDetailsMapper
 * @projectName mylite
 * @Data 2021/8/20 14:33
 */
public interface CourseDetailsMapper extends BaseMapper<CourseDetails> {

    IPage<CourseDetailsDTO> courseDetails(Page page, @Param("query") CourseDetailsReqDTO query);
}

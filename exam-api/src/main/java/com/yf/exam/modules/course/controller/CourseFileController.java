package com.yf.exam.modules.course.controller;

import com.yf.exam.core.api.ApiRest;
import com.yf.exam.core.api.controller.BaseController;
import com.yf.exam.core.utils.UploadUtils;
import com.yf.exam.core.utils.file.Office2Pdf;
import com.yf.exam.core.utils.video.ChangeVideo;
import com.yf.exam.modules.course.dto.request.CourseFileReqDTO;
import com.yf.exam.modules.course.entity.Courseware;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * @ClassName CourseFileController
 * @Description 课程中，文件上传和文件获取
 * @Author Mr.Fu
 * @Date 2021-08-26 15:00:25
 * @Version 1.0
 */
@Api
@RestController
@RequestMapping("/course/api/course/file")
public class CourseFileController extends BaseController {

    @Value("${web.upload-path}")
    private String fillePath;

    /**
     * 上传文件，格式为图片(.jpg,.png,.jpeg)
     * @param file
     * @return
     */
    @ApiOperation(value = "上传课件封面")
    @RequestMapping(value = "/img", method = {RequestMethod.POST})
    public ApiRest<Courseware> saveCourseCover(@RequestBody MultipartFile file) {

        //判断是否为图片
        Boolean compareImg = UploadUtils.fileCompareImg(file);
        String path="";
        if (compareImg){
            Courseware courseware=new Courseware();
            //上传的文件，并返回路径
            path= UploadUtils.saveFile(file, "courseCover", fillePath);
            courseware.setAddress(path);
            courseware.setViewUrl("");
            return super.success(courseware);
        }
        return super.failure();

    }

    /**
     * 上传文件，格式为office(.doc,.xls,.ppt,docx,xlsx,.pptx)
     * @param file
     * @return
     */
    @ApiOperation(value = "课件上传--office")
    @RequestMapping(value = "/courseware-office", method = {RequestMethod.POST})

    public ApiRest<Courseware> saveCoursewareFileOffice(@RequestBody MultipartFile file) {
        //判断是否为office
        Boolean compareOffice = UploadUtils.fileCompareOffice(file);
        //上传的文件，并返回路径
        String path="";
        if (compareOffice){
            Courseware courseware=new Courseware();
            //上传的文件，并返回路径
            path= UploadUtils.saveFile(file, "coursewareFile/office", fillePath);
            courseware.setAddress(path);
            courseware.setViewUrl(path.substring(0,path.lastIndexOf("."))+".pdf");

            String pathStr=path;
            //未优化   ===》   待优化中
            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    try {
                        //获取前缀
                        String prefixPath = pathStr.substring(0,pathStr.lastIndexOf("/"));
                        File filePath = new File(fillePath+pathStr);
                        String pdfSavePath = fillePath+prefixPath;
                        String openOfficeIp = "127.0.0.1";
                        Office2Pdf.Office2Pdf(filePath,pdfSavePath,openOfficeIp);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            };
            new Thread(runnable).start();
            return super.success(courseware);
        }
        return super.failure();

    }

    /**
     * 上传文件，格式为pdf(.pdf)
     * @param file
     * @return
     */
    @ApiOperation(value = "课件上传--pdf")
    @RequestMapping(value = "/courseware-pdf", method = {RequestMethod.POST})
    public ApiRest<Courseware> saveCoursewareFilePdf(@RequestBody MultipartFile file) {
        //判断是否为pdf
        Boolean comparePdf = UploadUtils.fileComparePdf(file);
        //上传的文件，并返回路径
        String path="";
        if (comparePdf){
            Courseware courseware=new Courseware();
            //上传的文件，并返回路径
            path= UploadUtils.saveFile(file, "coursewareFile/pdf", fillePath);
            courseware.setAddress(path);
            courseware.setViewUrl("");
            return super.success(courseware);
        }
        return super.failure();
    }

    /**
     * 上传文件，视频格式(.avi,.mp4,.ogv,.wmv,.mkv,.flv,.rm,.rmvb)
     * @param file
     * @return
     */
    @ApiOperation(value = "课件上传--视频")
    @RequestMapping(value = "/courseware-video", method = {RequestMethod.POST})
    public ApiRest<Courseware> saveCoursewareFileVideo(@RequestBody MultipartFile file) {

        if (UploadUtils.suffixName(file).equals(".wmv9") || UploadUtils.suffixName(file).equals(".rm") ||
        UploadUtils.suffixName(file).equals(".rmvb")){
            return super.failure("很抱歉，展示无法实现该格式上传和转化");
        }

        //判断是否为视频
        Boolean comparePdf = UploadUtils.fileCompareVideo(file);
        //上传的文件，并返回路径
        String path="";
        if (comparePdf){
            Courseware courseware=new Courseware();
            //上传的文件，并返回路径
            path= UploadUtils.saveFile(file, "coursewareFile/video", fillePath);
            courseware.setAddress(path);
            courseware.setViewUrl("");
            String pathStr=path;

            if (!UploadUtils.suffixName(file).equals(".mp4")){

                String pathMp4=pathStr.substring(0,pathStr.lastIndexOf("."))+".mp4";
                courseware.setViewUrl(pathMp4);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ChangeVideo.convert(fillePath+pathStr,fillePath+pathMp4);
                    }
                }).start();
            }
            return super.success(courseware);
        }


        return super.failure();


    }

    /**
     * 文件的删除
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "文件删除")
    @RequestMapping(value = "/file-delete", method = {RequestMethod.POST})
    public ApiRest<Boolean> deleteFile(@RequestBody CourseFileReqDTO reqDTO) {
        boolean b = FileSystemUtils.deleteRecursively(new File(fillePath+reqDTO.getPath()));
        return super.success(b);
    }

}

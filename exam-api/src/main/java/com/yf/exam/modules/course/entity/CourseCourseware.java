package com.yf.exam.modules.course.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
 * @ClassName CourseCourseware
 * @Description 课程课件实体类
 * @Author BlueAutumn
 * @Date 2021/8/23 9:44
 * @Version 1.0
 */
@Data
@TableName("el_course_courseware")
public class CourseCourseware extends Model<CourseCourseware> {

    private static final long serialVersionUID = -8596982501485064176L;

    @TableId(value = "ID", type = IdType.ASSIGN_ID)
    private String id;

    @TableField("course_id")
    private String courseId;

    @TableField("courseware_id")
    private String coursewareId;

}

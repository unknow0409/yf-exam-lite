package com.yf.exam.modules.exam.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yf.exam.core.api.ApiRest;
import com.yf.exam.core.api.controller.BaseController;
import com.yf.exam.core.api.dto.BaseIdReqDTO;
import com.yf.exam.core.api.dto.BaseIdsReqDTO;
import com.yf.exam.core.api.dto.BaseStateReqDTO;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.exam.dto.*;
import com.yf.exam.modules.exam.dto.request.ExamDetailsReqDTO;
import com.yf.exam.modules.exam.dto.request.ExamPagingReqDTO;
import com.yf.exam.modules.exam.dto.request.ExamSaveReqDTO;
import com.yf.exam.modules.exam.dto.request.ExamStatisticsReqDTO;
import com.yf.exam.modules.exam.dto.response.ExamOnlineRespDTO;
import com.yf.exam.modules.exam.dto.response.ExamReviewRespDTO;
import com.yf.exam.modules.exam.entity.Exam;
import com.yf.exam.modules.exam.service.*;
import com.yf.exam.modules.paper.dto.PaperQuDTO;
import com.yf.exam.modules.paper.dto.ext.PaperQuDetailDTO;
import com.yf.exam.modules.paper.dto.request.PaperQuQueryDTO;
import com.yf.exam.modules.paper.service.PaperService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;

/**
* <p>
* 考试控制器
* </p>
*
* @author 聪明笨狗
* @since 2020-07-25 16:18
*/
@Api(tags={"考试"})
@RestController
@RequestMapping("/exam/api/exam/exam")
public class ExamController extends BaseController {

    @Autowired
    private ExamService baseService;

    @Autowired
    private ExamGradeService examGradeService;

    @Autowired
    private PaperService paperService;

    @Autowired
    private ExamSearchService examSearchService;

    @Autowired
    private ExamStatisticsService examStatisticsService;

    @Autowired
    private ExamDetailsService examDetailsService;

    @Autowired
    private  ExamDetailsSearchService examDetailsSearchService;

    /**
    * 添加或修改
    * @param reqDTO
    * @return
    */
    @ApiOperation(value = "添加或修改")
    @RequestMapping(value = "/save", method = { RequestMethod.POST})
    public ApiRest save(@RequestBody ExamSaveReqDTO reqDTO) {
        //复制参数
        baseService.save(reqDTO);
        return super.success();
    }

    /**
    * 批量删除
    * @param reqDTO
    * @return
    */
    @ApiOperation(value = "批量删除")
    @RequestMapping(value = "/delete", method = { RequestMethod.POST})
    public ApiRest edit(@RequestBody BaseIdsReqDTO reqDTO) {
        //根据ID删除
        baseService.removeByIds(reqDTO.getIds());
        return super.success();
    }

    /**
    * 查找详情
    * @param reqDTO
    * @return
    */
    @ApiOperation(value = "查找详情")
    @RequestMapping(value = "/detail", method = { RequestMethod.POST})
    public ApiRest<ExamDTO> find(@RequestBody BaseIdReqDTO reqDTO) {
        ExamSaveReqDTO dto = baseService.findDetail(reqDTO.getId());
        return super.success(dto);
    }

    /**
     * 查找详情
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "查找详情")
    @RequestMapping(value = "/state", method = { RequestMethod.POST})
    public ApiRest state(@RequestBody BaseStateReqDTO reqDTO) {

        QueryWrapper<Exam> wrapper = new QueryWrapper<>();
        wrapper.lambda().in(Exam::getId, reqDTO.getIds());
        Exam exam = new Exam();
        exam.setState(reqDTO.getState());
        exam.setUpdateTime(new Date());

        baseService.update(exam, wrapper);
        return super.success();
    }

    /**
     * 查找详情
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "查找详情")
    @RequestMapping(value = "/findExamAnswer", method = { RequestMethod.POST})
    public ApiRest findExamAnswer(@RequestBody ExamAnswerDTO reqDTO) {
        ExamAnswerDTO dto = examGradeService.findExamAnswer(reqDTO);
        return super.success(dto);
    }


    /**
     * 分页查找
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "考试视角")
    @RequestMapping(value = "/online-paging", method = { RequestMethod.POST})
    public ApiRest<IPage<ExamOnlineRespDTO>> myPaging(@RequestBody PagingReqDTO<ExamDTO> reqDTO) {

        //分页查询并转换
        IPage<ExamOnlineRespDTO> page = baseService.onlinePaging(reqDTO);
        return super.success(page);
    }

    /**
    * 分页查找
    * @param reqDTO
    * @return
    */
    @ApiOperation(value = "分页查找")
    @RequestMapping(value = "/paging", method = { RequestMethod.POST})
    public ApiRest<IPage<ExamDTO>> paging(@RequestBody PagingReqDTO<ExamDTO> reqDTO) {

        //分页查询并转换
        IPage<ExamDTO> page = baseService.paging(reqDTO);

        return super.success(page);
    }

    /**
     * 分页查找-开放部门参数支持传具体部门ID
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "分页查找")
    @RequestMapping(value = "/paging-depart", method = { RequestMethod.POST})
    public ApiRest<IPage<ExamDTO>> pagingDepart(@RequestBody PagingReqDTO<ExamPagingReqDTO> reqDTO) {

        //分页查询并转换
        IPage<ExamDTO> page = baseService.pagingDepart(reqDTO);

        return super.success(page);
    }


    /**
     * 分页查找
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "待阅试卷")
    @RequestMapping(value = "/review-paging", method = { RequestMethod.POST})
    public ApiRest<IPage<ExamReviewRespDTO>> reviewPaging(@RequestBody PagingReqDTO<ExamDTO> reqDTO) {

        //分页查询并转换
        IPage<ExamReviewRespDTO> page = baseService.reviewPaging(reqDTO);

        return super.success(page);
    }

    /**
     * 分页查找
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "试卷信息")
    @RequestMapping(value = "/grading", method = { RequestMethod.POST})
    public ApiRest<IPage<ExamGradeDTO>> grading(@RequestBody PagingReqDTO<ExamGradeDTO> reqDTO) {

        IPage<ExamGradeDTO> page = examGradeService.grading(reqDTO);
        System.out.println(page);
        return super.success(page);
    }

    /**
     * 分页查找
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "试卷题目信息")
    @RequestMapping(value = "/findExamSubject", method = { RequestMethod.POST})
    public ApiRest<IPage<ExamSubjectDTO>> findExamSubject(@RequestBody PagingReqDTO<ExamSubjectDTO> reqDTO) {
        IPage<ExamSubjectDTO> page = examGradeService.findExamSubject(reqDTO);
        System.out.println(page);
        return super.success(page);
    }

    /**
     * 提交阅卷
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "提交阅卷")
    @RequestMapping(value = "/updatePaperScore", method = { RequestMethod.POST})
    public ApiRest<PaperQuDetailDTO> updatePaperScore(@RequestBody PaperQuDTO reqDTO) {
        paperService.updatePaperScore(reqDTO);
        return super.success();
    }

    /**
     * 确认阅卷
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "确认阅卷")
    @RequestMapping(value = "/updateUserScore", method = { RequestMethod.POST})
    public ApiRest<PaperQuDetailDTO> updatePaperScore(@RequestBody PaperQuQueryDTO reqDTO) {
        paperService.updateUserScore(reqDTO);
        return super.success();
    }

    /**
     * 按名称搜索
     *
     * @return
     */
    @ApiOperation(value = "按考试名称搜索")
    @RequestMapping(value ="/search",method = {RequestMethod.POST})
    public ApiRest<List<String>> search(@RequestBody ExamDTO query){

       List<String> srt = examSearchService.title(query);
        return super.success(srt);
    }

    /**
     * 考试统计
     *
     * @return
     */
    @ApiOperation(value = "考试情况统计")
    @RequestMapping(value ="/examStatistics",method = {RequestMethod.POST})
    public ApiRest<IPage<ExamStatisticsDTO>>examStatistics(@RequestBody PagingReqDTO<ExamStatisticsReqDTO> query){

        IPage<ExamStatisticsDTO> statistics = examStatisticsService.statistics(query);

        return super.success(statistics);

    }

    /**
     * 考试详情
     *
     * @return
     */
    @ApiOperation(value = "考试详情")
    @RequestMapping(value ="/examDetails",method = {RequestMethod.POST})
    public  ApiRest<IPage<ExamDetailsDTO>> examDetail(@RequestBody PagingReqDTO<ExamDetailsReqDTO> query){

        IPage<ExamDetailsDTO> testData = examDetailsService.details(query);

        return super.success(testData);
    }

    /**
     * 考试详情查询
     *
     * @return
     */
     @ApiOperation(value = "考试人员姓名查询")
     @RequestMapping(value ="/examDetailsSearch",method = {RequestMethod.POST})
    public  ApiRest<List<String>> examDetailsSearch(@RequestBody ExamDetailsReqDTO query) {

         List<String> searchData = examDetailsSearchService.detailsSearch(query);

         return super.success(searchData);
     }
}

package com.yf.exam.modules.course.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName CoursewareCategoryDTO
 * @Description 课件类型数据传输类
 * @Author BlueAutumn
 * @Date 2021/8/27 9:26
 * @Version 1.0
 */
@Data
@ApiModel(value = "课件类目", description = "课件类目")
public class CoursewareCategoryDTO implements Serializable {

    private static final long serialVersionUID = -8065938891978315958L;

    @ApiModelProperty(value = "主键ID", required = true)
    private String id;

    @ApiModelProperty(value = "课件类型名称", required = true)
    private String name;

}

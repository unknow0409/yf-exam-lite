package com.yf.exam.modules.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.exam.modules.course.entity.CourseCourseware;

/**
 *@InterfaceName CourseCoursewareMapper
 *@Description 课程课件Mapper
 *@Author BlueAutumn
 *@Date 2021/8/23 9:50
 *@Version 1.0
 */
public interface CourseCoursewareMapper extends BaseMapper<CourseCourseware> {
}
package com.yf.exam.modules.course.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.course.dto.CourseDetailsDTO;
import com.yf.exam.modules.course.dto.request.CourseDetailsReqDTO;
import com.yf.exam.modules.course.entity.CourseDetails;
import com.yf.exam.modules.course.mapper.CourseDetailsMapper;
import com.yf.exam.modules.course.service.CourseDetailsService;
import org.springframework.stereotype.Service;

/**
 * @author lgy
 * @title :CourseDetailsServiceImpl
 * @projectName mylite
 * @Data 2021/8/20 14:35
 */
@Service
public class CourseDetailsServiceImpl extends ServiceImpl<CourseDetailsMapper, CourseDetails> implements CourseDetailsService {

    @Override
   public IPage<CourseDetailsDTO> courseDetails(PagingReqDTO<CourseDetailsReqDTO> reqDTO){

        Page page = new Page(reqDTO.getCurrent(), reqDTO.getSize());

        IPage<CourseDetailsDTO> detailsData = baseMapper.courseDetails(page, reqDTO.getParams());

        return detailsData;

    }
}

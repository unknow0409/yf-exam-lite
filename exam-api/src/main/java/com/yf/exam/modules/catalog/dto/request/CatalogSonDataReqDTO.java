package com.yf.exam.modules.catalog.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName CatalogSonDataReqDTO
 * @Description 获取分类子表的接受类
 * @Author Mr.Fu
 * @Date 2021-09-28 09:40:10
 * @Version 1.0
 */
@Data
@ApiModel(value = "获取分类子表的接受类", description = "获取分类子表的接受类")
public class CatalogSonDataReqDTO {
    @ApiModelProperty(value = "分类表ID", required = true)
    private String catalogId;
    @ApiModelProperty(value = "分类表编码", required = true)
    private String catalogCode;
}

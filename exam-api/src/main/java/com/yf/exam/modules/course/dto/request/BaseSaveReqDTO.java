package com.yf.exam.modules.course.dto.request;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName BaseSaveReqDTO
 * @Description 基础增改请求类
 * @Author BlueAutumn
 * @Date 2021/8/23 17:02
 * @Version 1.0
 */
@Data
@ApiModel(value = "基础增改请求类", description = "基础增改请求类")
public class BaseSaveReqDTO<T> implements Serializable {

    private static final long serialVersionUID = -2515214094317124616L;

    private T params;

}

package com.yf.exam.modules.user.course.entity;

import lombok.Data;

import java.util.Date;

/**
 *
 * <p>
 *  课程实体类
 *  </p>
 *
 * @author lgy
 * @title :Course
 * @projectName mylite
 * @Data 2021/8/19 11:13
 */
@Data
public class Course  {
    /**
     * 课程ID
     */
    private String id;

    /**
     * 课程名称
     */
    private String courseName;

    /**
     * 课程封面地址
     */
    private String img;

    /**
     * 课程简介
     */
    private String describe;

    /**
     * 课程分类
     */
    private String courseCategory;

    /**
     * 课时
     */
    private Double courseTime;

    /**
     * 课程创建时间
     */
    private Date createTime;

    /**
     * 课程修改时间
     */
    private Date changeTime;

    /**
     * 课程积分
     */
    private Double courseIntegral;

    /**
     * 课程是否必修，0非必修,1必修
     */
    private Integer isNecessary;

    /**
     * 课程开放权限，0完全开放，1指定用户开放
     */
    private Integer openType;

}

package com.yf.exam.modules.catalog.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName CatalogSonDTO
 * @Description 分类子表
 * @Author Mr.Fu
 * @Date 2021-09-22 09:03:17
 * @Version 1.0
 */
@Data
@ApiModel(value="分类子表", description="分类子表")
public class CatalogSonDTO implements Serializable {

    @ApiModelProperty(value = "ID", required=true)
    private String id;

    @ApiModelProperty(value = "分类ID", required=true)
    private String catalogId;

    @ApiModelProperty(value = "父类ID", required=true)
    private String parentId;

    @ApiModelProperty(value = "分类子表名称", required=true)
    private String catalogSonName;

    @ApiModelProperty(value = "分类子表描述", required=true)
    private String catalogSonDescription;

    @ApiModelProperty(value = "分类子表编码", required=true)
    private String catalogSonCode;
}

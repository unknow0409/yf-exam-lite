package com.yf.exam.modules.stat.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.stat.dto.StatUserDTO;
import com.yf.exam.modules.stat.dto.request.StatUserQueryReqDTO;
import com.yf.exam.modules.stat.entity.StatUser;

/**
 *@InterfaceName StatUserService
 *@Description 用户统计业务类
 *@Author BlueAutumn
 *@Date 2021/8/16 16:50
 *@Version 1.0
 */
public interface StatUserService extends IService<StatUser> {

    /**
     * 分页查询及筛选
     * @param reqDTO
     * @return
     */
    IPage<StatUserDTO> paging(PagingReqDTO<StatUserQueryReqDTO> reqDTO);
}
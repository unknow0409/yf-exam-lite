package com.yf.exam.modules.course.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.apache.xmlbeans.impl.xb.xsdschema.WhiteSpaceDocument;

/**
 * @author lgy
 * @title :CoursewareDetailsReqDTO
 * @projectName mylite
 * @Data 2021/8/26 14:52
 */
@Data
@ApiModel(value = "课件详情请求类 ",description = "课件详情请求类")
public class CoursewareDetailsReqDTO {

        @ApiModelProperty(value = "课件类型")
        private String coursewareCategory;

        @ApiModelProperty(value = "课件名称")
        private String coursewareName;

        @ApiModelProperty(value = "课程类目")
        private String courseCategory;
}

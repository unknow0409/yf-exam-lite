package com.yf.exam.modules.course.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * @author lgy
 * @title :CourseDetailsDTO
 * @projectName mylite
 * @Data 2021/8/20 14:37
 */
@Data
@ApiModel(value = "课程详情", description = "课程详情")
public class CourseDetailsDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户名", required = true)
    private String userName;

    @ApiModelProperty(value = "课程名", required = true)
    private String courseName;

    @ApiModelProperty(value = "已学课件数", required = true)
    private Integer studiedCourse;

    @ApiModelProperty(value = "总学课件数", required = true)
    private Integer totalCourse;

    @ApiModelProperty(value = "学习状态", required = true)
    private String learningState;

    @ApiModelProperty(value = "更新时间", required = true)
    private String changeTime;


}

package com.yf.exam.modules.paper.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.modules.paper.entity.PaperUserState;
import com.yf.exam.modules.paper.mapper.PaperUserStateMapper;
import com.yf.exam.modules.paper.service.PaperUserStateService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaperUserStateServicelmpl extends ServiceImpl<PaperUserStateMapper, PaperUserState> implements PaperUserStateService {
    @Override
    public List<PaperUserState> list(String userID) {
        return baseMapper.list(userID);
    }
}

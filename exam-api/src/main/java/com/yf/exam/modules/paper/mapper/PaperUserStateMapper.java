package com.yf.exam.modules.paper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.exam.modules.paper.entity.PaperUserState;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PaperUserStateMapper extends BaseMapper<PaperUserState> {
    /**
     * 未答试卷列表响应类
     * @param userId
     * @return
     */
    List<PaperUserState> list(@Param("userId") String userId);
}

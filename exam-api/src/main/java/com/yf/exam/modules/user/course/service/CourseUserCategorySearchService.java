package com.yf.exam.modules.user.course.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.user.course.dto.CourseDTO;
import com.yf.exam.modules.user.course.entity.Course;

import java.util.List;

/**
 * @author lgy
 * @title :CourseUserCategorySearchService
 * @projectName mylite
 * @Data 2021/9/14 9:51
 */
public interface CourseUserCategorySearchService extends IService<Course> {

    List<String> courseUsercategorySearch(CourseDTO query);
}

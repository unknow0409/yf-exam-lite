package com.yf.exam.modules.user.course.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yf.exam.core.api.ApiRest;
import com.yf.exam.core.api.controller.BaseController;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.user.course.dto.CourseDTO;
import com.yf.exam.modules.user.course.dto.request.CourseUserDetailsReqDTO;
import com.yf.exam.modules.user.course.service.CourseUserCategorySearchService;
import com.yf.exam.modules.user.course.service.CourseUserDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程学生端控制器
 * </p>
 *
 * @author lgy
 * @title :CourseUserController
 * @projectName mylite
 * @Data 2021/8/19 11:06
 */
@Api(tags = {"课程"})
@RestController
@RequestMapping("/course/api/user/course")
public class CourseUserController extends BaseController {

    @Autowired
    private CourseUserDetailsService courseDetailsService;

    @Autowired
    private CourseUserCategorySearchService courseUserCategorySearchService;

    /**
     * 课程详情
     *
     * @return
     */
    @ApiOperation(value = "课程详情")
    @RequestMapping(value = "/courseDetails", method = {RequestMethod.POST})
    public ApiRest<IPage<CourseDTO>> courseDetail(@RequestBody PagingReqDTO<CourseUserDetailsReqDTO> query) {

        IPage<CourseDTO> courseData = courseDetailsService.courseDetails(query);

        return super.success(courseData);
    }

    /**
     * 获取课程类别，并按类别进行搜索
     * @return
     *
     */
    @ApiOperation(value = "课程类别获取，搜索")
    @RequestMapping(value = "/courseUserCategorySearch",method = {RequestMethod.POST})
    public ApiRest <List<String>> courseUserCategorySearch(@RequestBody CourseDTO query){

        List<String> categorySearchData = courseUserCategorySearchService.courseUsercategorySearch(query);

        return super.success(categorySearchData);

    }
}

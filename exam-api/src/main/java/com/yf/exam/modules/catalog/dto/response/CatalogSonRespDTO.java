package com.yf.exam.modules.catalog.dto.response;

import com.yf.exam.modules.catalog.dto.CatalogSonDTO;
import com.yf.exam.modules.catalog.entity.CatalogSon;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

/**
 * @ClassName CatalogSonRespDTO
 * @Description 分类子表--相应类
 * @Author Mr.Fu
 * @Date 2021-09-22 13:46:24
 * @Version 1.0
 */
@Data
@ApiModel(value="分类子表响应类", description="分类子表响应类")
public class CatalogSonRespDTO extends CatalogSonDTO{
    private List<CatalogSonRespDTO> sonList;
}

package com.yf.exam.modules.course.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.course.entity.CoursewareRecord;

/**
 * @author lgy
 * @title :CoursewareRecordService
 * @projectName mylite
 * @Data 2021/8/27 17:25
 */
public interface CoursewareRecordService extends IService<CoursewareRecord> {
}

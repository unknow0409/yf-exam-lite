package com.yf.exam.modules.catalog.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName CatalogSearchDTO
 * @Description 分类字典查询数据传输类
 * @Author BlueAutumn
 * @Date 2021/9/22 9:42
 * @Version 1.0
 */
@Data
@ApiModel(value = "分类字典查询数据传输类", description = "分类字典查询数据传输类")
public class CatalogSearchReqDTO implements Serializable {

    private static final long serialVersionUID = 2472431338395950071L;

    @ApiModelProperty(value = "根据编码或者名称查询", required = true)
    private String search;
}

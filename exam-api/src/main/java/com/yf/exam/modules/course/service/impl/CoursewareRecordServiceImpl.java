package com.yf.exam.modules.course.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.modules.course.entity.Courseware;
import com.yf.exam.modules.course.entity.CoursewareRecord;
import com.yf.exam.modules.course.mapper.CoursewareDetailsMapper;
import com.yf.exam.modules.course.mapper.CoursewareRecordMapper;
import com.yf.exam.modules.course.service.CoursewareDetailsService;
import com.yf.exam.modules.course.service.CoursewareRecordService;
import org.springframework.stereotype.Service;

/**
 * @author lgy
 * @title :CoursewareRecordServiceImpl
 * @projectName mylite
 * @Data 2021/8/27 17:22
 */
@Service
public class CoursewareRecordServiceImpl extends ServiceImpl<CoursewareRecordMapper, CoursewareRecord> implements CoursewareRecordService {
}

package com.yf.exam.modules.user.course.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.user.course.dto.CourseDTO;
import com.yf.exam.modules.user.course.dto.request.CourseUserDetailsReqDTO;
import com.yf.exam.modules.user.course.entity.Course;

/**
 * @author lgy
 * @title :CourseUserDetailsService
 * @projectName mylite
 * @Data 2021/8/19 15:00
 */
public interface CourseUserDetailsService extends IService<Course> {

    IPage<CourseDTO> courseDetails(PagingReqDTO<CourseUserDetailsReqDTO> reqDTO);
}

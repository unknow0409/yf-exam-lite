package com.yf.exam.modules.paper.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="用户未交试卷", description="试用户未交试卷")
public class PaperUserStateDTO {

    @ApiModelProperty(value = "试卷id")
    private String paperId;

    @ApiModelProperty(value = "试卷标题")
    private String title;

    @ApiModelProperty(value = "试卷题数")
    private int quCount;

    @ApiModelProperty(value = "已做题数")
    private int rightCount;
}

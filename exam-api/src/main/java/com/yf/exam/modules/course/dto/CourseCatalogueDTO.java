package com.yf.exam.modules.course.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName CourseCatalogue
 * @Description 课程目录数据传输类
 * @Author BlueAutumn
 * @Date 2021/8/20 9:28
 * @Version 1.0
 */
@Data
@ApiModel
public class CourseCatalogueDTO implements Serializable {

    private static final long serialVersionUID = 8809802331955279506L;

    @ApiModelProperty(value = "课程目录ID", required = true)
    private String id;

    @ApiModelProperty(value = "课程ID", required = true)
    private String courseId;

    @ApiModelProperty(value = "目录名称", required = true)
    private String catalogueName;

    @ApiModelProperty(value = "排序顺序", required = true)
    private Integer sorting;

}

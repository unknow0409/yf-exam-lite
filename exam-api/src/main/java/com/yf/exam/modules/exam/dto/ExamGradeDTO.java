package com.yf.exam.modules.exam.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@ApiModel(value = "考试批改",description = "考试批改")
public class ExamGradeDTO implements Serializable {

    @ApiModelProperty(value = "试卷ID")
    private String paperId;

    @ApiModelProperty(value = "考试人名字")
    private String realName;

    @ApiModelProperty(value = "考试名字")
    private String title;

    @ApiModelProperty(value = "当前总分数")
    private int userScore;

    @ApiModelProperty(value = "单选题得分")
    private int radioScore;

    @ApiModelProperty(value = "多选题得分")
    private int multiScore;

    @ApiModelProperty(value = "判断题得分")
    private int judgeScore;

    @ApiModelProperty(value = "简答题得分")
    private int answerScore;

    @ApiModelProperty(value = "创建时间")
    private Timestamp createTime;

    @ApiModelProperty(value = "批改状态")
    private  int reviewCount;

    @ApiModelProperty(value = "答题数")
    private int answerCount;

    @ApiModelProperty(value = "试卷状态")
    private int state;
}

package com.yf.exam.modules.qa.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

@Data
@TableName("el_course_qa")
public class Qa {
	/**
	 * ID
	 */
	@TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;
	/**
	 * 所属课程ID
	 */
	@TableField("course_id")
	private String courseId;
	/**
	 * 所属课件ID
	 */
	@TableField("courseware_id")
	private String coursewareId;
	/**
	 * 提问人
	 */
	@TableField("question_people")
	private String questionPeople;
	/**
	 * 问题标题
	 */
	private String title;
	/**
	 * 创建时间
	 */
	@TableField("creation_time")
	private Date creationTime;
	/**
	 * 提问内容
	 */
	
	private String content;
	/**
	 * 问题答复
	 */
	private String answer;
	/**
	 * 回复状态
	 */
	private int state;

}

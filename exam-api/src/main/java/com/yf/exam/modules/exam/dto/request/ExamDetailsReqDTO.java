package com.yf.exam.modules.exam.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author lgy
 * @title :ExamDetailsReqDTO
 * @projectName mylite
 * @Data 2021/8/16 18:01
 */
@Data
@ApiModel(value = "考试详情请求类", description = "考试详情请求类")
public class ExamDetailsReqDTO {

    @ApiModelProperty(value = "考试日期")
    private String examDate;

    @ApiModelProperty(value = "考试科目")
    private String subject;

    @ApiModelProperty(value = "考试姓名")
    private String examName;

    @ApiModelProperty(value = "分数区间最小值")
    private double minScore;

    @ApiModelProperty(value = "分数区间最大值")
    private double maxScore;
}

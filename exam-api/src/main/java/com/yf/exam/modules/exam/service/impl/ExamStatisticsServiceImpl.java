package com.yf.exam.modules.exam.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.exam.dto.ExamStatisticsDTO;
import com.yf.exam.modules.exam.dto.request.ExamStatisticsReqDTO;
import com.yf.exam.modules.exam.entity.ExamStatistics;
import com.yf.exam.modules.exam.mapper.ExamStatisticsMapper;
import com.yf.exam.modules.exam.service.ExamStatisticsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lgy
 * @Data2021/8/11 15:49
 */

@Service
public class ExamStatisticsServiceImpl extends ServiceImpl<ExamStatisticsMapper, ExamStatistics> implements ExamStatisticsService {

    @Override
    public IPage<ExamStatisticsDTO> statistics(PagingReqDTO<ExamStatisticsReqDTO> reqDTO) {

        //创建分页对象
        Page page = new Page(reqDTO.getCurrent(), reqDTO.getSize());

        IPage<ExamStatisticsDTO> statistics = baseMapper.statistics(page, reqDTO.getParams());
        return statistics;
    }

}

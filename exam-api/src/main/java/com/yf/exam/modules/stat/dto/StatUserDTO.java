package com.yf.exam.modules.stat.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName StatUserDTO
 * @Description 用户统计请求类
 * @Author BlueAutumn
 * @Date 2021/8/16 16:34
 * @Version 1.0
 */
@Data
@ApiModel(value = "用户统计", description = "用户统计")
public class StatUserDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID", required = true)
    private String id;

    @ApiModelProperty(value = "统计时间", required = true)
    private Date statTime;

    @ApiModelProperty(value = "活跃用户数", required = true)
    private Integer activeUser;

    @ApiModelProperty(value = "新增用户数", required = true)
    private Integer newUser;

    @ApiModelProperty(value = "总用户数", required = true)
    private Integer totalUser;
}

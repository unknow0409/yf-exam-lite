package com.yf.exam.modules.exam.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.exam.dto.ExamAnswerDTO;
import com.yf.exam.modules.exam.dto.ExamGradeDTO;
import com.yf.exam.modules.exam.dto.ExamSubjectDTO;
import com.yf.exam.modules.exam.entity.ExamGrade;
import com.yf.exam.modules.exam.mapper.ExamGradeMapper;
import com.yf.exam.modules.exam.service.ExamGradeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamGradeServicelmpl extends ServiceImpl<ExamGradeMapper, ExamGrade> implements ExamGradeService {
    @Override
    public IPage<ExamGradeDTO> grading(PagingReqDTO<ExamGradeDTO> reqDTO) {
        // 创建分页对象
        Page page = new Page(reqDTO.getCurrent(), reqDTO.getSize());
        //查询参数
        String realName = reqDTO.getParams().getRealName();
        String examName=reqDTO.getParams().getTitle();
        return baseMapper.grading(page,realName,examName);
    }

    @Override
    public IPage<ExamSubjectDTO> findExamSubject(PagingReqDTO<ExamSubjectDTO> reqDTO) {
        // 创建分页对象
        Page page = new Page(reqDTO.getCurrent(), reqDTO.getSize());
        return baseMapper.findExamSubject(page,reqDTO.getParams().getPaperId());
    }

    @Override
    public ExamAnswerDTO findExamAnswer(ExamAnswerDTO reqDTO) {
        return baseMapper.findExamAnswer(reqDTO.getPaperId(),reqDTO.getQuId());
    }
}

package com.yf.exam.modules.course.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName CatalogueCoursewareDTO
 * @Description 目录课件数据传输类
 * @Author BlueAutumn
 * @Date 2021/8/20 9:10
 * @Version 1.0
 */
@Data
@ApiModel(value = "目录课件", description = "目录课件")
public class CatalogueCoursewareDTO implements Serializable {

    private static final long serialVersionUID = -4040060427411501375L;

    @ApiModelProperty(value = "ID", required = true)
    private String id;

    @ApiModelProperty(value = "目录ID", required = true)
    private String catalogueId;

    @ApiModelProperty(value = "课件ID", required = true)
    private String coursewareId;

    @ApiModelProperty(value = "排序顺序", required = true)
    private Integer sorting;

    @ApiModelProperty(value = "课程学习时长（分钟）", required = true)
    private Double coursewareTime;

    @ApiModelProperty(value = "课程学习积分", required = true)
    private Double coursewareIntegral;

}

package com.yf.exam.modules.course.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName Course
 * @Description 课程实体类
 * @Author BlueAutumn
 * @Date 2021/8/19 10:40
 * @Version 1.0
 */
@Data
@TableName("el_course")
public class Course extends Model<Course> {

    private static final long serialVersionUID = -2353985186172707983L;

    /**
     * 课程ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 课程名称
     */
    @TableField("course_name")
    private String courseName;

    /**
     * 课程封面地址
     */
    private String img;

    /**
     * 课程简介
     */
    private String description;

    /**
     * 课程分类
     */
    @TableField("course_category")
    private String courseCategory;

    /**
     * 课时
     */
    @TableField("course_time")
    private Double courseTime;

    /**
     * 课程创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 课程修改时间
     */
    @TableField("change_time")
    private Date changeTime;

    /**
     * 课程积分
     */
    @TableField("course_integral")
    private Double courseIntegral;

    /**
     * 课程是否必修，0非必修,1必修
     */
    @TableField("is_necessary")
    private Integer isNecessary;

    /**
     * 课程开放权限，1完全开放，2指定部门开放
     */
    @TableField("open_type")
    private Integer openType;

}

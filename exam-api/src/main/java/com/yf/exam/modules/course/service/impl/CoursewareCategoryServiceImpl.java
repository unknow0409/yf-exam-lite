package com.yf.exam.modules.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.modules.course.dto.response.CoursewareCategoryRespDTO;
import com.yf.exam.modules.course.entity.CoursewareCategory;
import com.yf.exam.modules.course.mapper.CoursewareCategoryMapper;
import com.yf.exam.modules.course.service.CoursewareCategoryService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName CoursewareCategoryServiceImpl
 * @Description 课件类目业务实现类
 * @Author BlueAutumn
 * @Date 2021/8/26 17:42
 * @Version 1.0
 */
@Service
public class CoursewareCategoryServiceImpl extends ServiceImpl<CoursewareCategoryMapper, CoursewareCategory> implements CoursewareCategoryService {

    @Override
    public CoursewareCategoryRespDTO listName() {
        CoursewareCategoryRespDTO dto = new CoursewareCategoryRespDTO();

        QueryWrapper<CoursewareCategory> wrapper = new QueryWrapper<>();
        wrapper.select("name");
        List<String> nameList = new ArrayList<>();
        List<CoursewareCategory> list = this.list(wrapper);
        for (CoursewareCategory coursewareCategory : list) {
            nameList.add(coursewareCategory.getName());
        }
        dto.setCoursewareCategoryList(nameList);

        return dto;
    }
}

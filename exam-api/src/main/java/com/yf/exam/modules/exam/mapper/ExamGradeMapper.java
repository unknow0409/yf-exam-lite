package com.yf.exam.modules.exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yf.exam.modules.exam.dto.ExamAnswerDTO;
import com.yf.exam.modules.exam.dto.ExamDTO;
import com.yf.exam.modules.exam.dto.ExamGradeDTO;
import com.yf.exam.modules.exam.dto.ExamSubjectDTO;
import com.yf.exam.modules.exam.entity.ExamGrade;
import org.apache.ibatis.annotations.Param;

public interface ExamGradeMapper extends BaseMapper<ExamGrade> {

    IPage<ExamGradeDTO> grading(Page page, @Param("realName") String realName,@Param("examName")String examName);

    IPage<ExamSubjectDTO> findExamSubject(Page page, @Param("paperId") String paperId);

    ExamAnswerDTO findExamAnswer(@Param("paperId") String paperId,@Param("quId") String quId);
}

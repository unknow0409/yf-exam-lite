package com.yf.exam.modules.sys.pdf.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface SysPdfService {

    byte[] previewPdf(String filePath, HttpServletRequest request, HttpServletResponse response);

}

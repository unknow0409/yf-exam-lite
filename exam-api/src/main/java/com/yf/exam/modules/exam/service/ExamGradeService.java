package com.yf.exam.modules.exam.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.exam.dto.ExamAnswerDTO;
import com.yf.exam.modules.exam.dto.ExamGradeDTO;
import com.yf.exam.modules.exam.dto.ExamSubjectDTO;
import com.yf.exam.modules.exam.entity.ExamDepart;
import com.yf.exam.modules.exam.entity.ExamGrade;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ExamGradeService extends IService<ExamGrade> {

    IPage<ExamGradeDTO> grading(PagingReqDTO<ExamGradeDTO> reqDTO);

    IPage<ExamSubjectDTO> findExamSubject(PagingReqDTO<ExamSubjectDTO> reqDTO);

    ExamAnswerDTO findExamAnswer(ExamAnswerDTO reqDTO);

}

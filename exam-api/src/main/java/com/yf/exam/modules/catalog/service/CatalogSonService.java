package com.yf.exam.modules.catalog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.catalog.dto.request.CatalogSonDataReqDTO;
import com.yf.exam.modules.catalog.dto.response.CatalogSonRespDTO;
import com.yf.exam.modules.catalog.entity.CatalogSon;
import com.yf.exam.modules.course.dto.response.CourseCategoryTreeRespDTO;

import java.util.List;

/**
 * @ClassName CatalogSonService
 * @Description 分类子表
 * @Author Mr.Fu
 * @Date 2021-09-22 09:08:37
 * @Version 1.0
 */

public interface CatalogSonService extends IService<CatalogSon> {
    /**
     * 分类子表的递归删除
     * @param catalogSon
     */
    void delete(CatalogSon catalogSon);

    /**
     * 分类子表的递归查询
     * @param reqDTO
     * @return
     */
    List<CatalogSonRespDTO> findAllData(CatalogSonDataReqDTO reqDTO);

    List<CourseCategoryTreeRespDTO> findTree(String code);
}

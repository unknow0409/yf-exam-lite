package com.yf.exam.modules.course.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.modules.course.dto.CourseDTO;
import com.yf.exam.modules.course.entity.Course;
import com.yf.exam.modules.course.mapper.CourseSearchMapper;
import com.yf.exam.modules.course.service.CourseSearchService;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lgy
 * @title :CourseSearchServiceImpl
 * @projectName mylite
 * @Data 2021/8/20 8:57
 */
@Service
public class CourseSearchServiceImpl extends ServiceImpl<CourseSearchMapper, Course> implements CourseSearchService {

    @Override
    public List<String> courseSearch(CourseDTO query){
        return baseMapper.courseSearch(query);
    }
}

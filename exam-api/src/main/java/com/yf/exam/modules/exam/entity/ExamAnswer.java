package com.yf.exam.modules.exam.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 用户试卷答题实体类
 * </p>
 */
@Data
public class ExamAnswer {

    /**
     * 试卷id
     */
    private String paperId;

    /**
     * 问题题目id
     */
    private String quId;

    /**
     * 单题总分
     */
    private String score;

    /**
     * 实际得分
     */
    private int actualScore;

    /**
     * 是否正确
     */
    private int isRight;
    /**
     * 用户解析
     */
    private String examAnalysis;

    /**
     * 正确解析
     */
    private String rightAnalysis;

    /**
     * 用户选项
     */
    private String examAbc;

    /**
     * 正确选项
     */
    private String rightAbc;
}

package com.yf.exam.modules.exam.entity;


/**
 * @author lgy
 * @title :ExamDetails
 * @projectName mylite
 * @Data 2021/8/16 15:21
 */
public class ExamDetails {


    /**
     * 试卷ID
     */
    private String paperId;

    /**
     * 考试名称
     */
    private String title;

    /**
     * 考试日期
     */
    private String date;

    /**
     * 考试时间
     */
    private String time;

    /**
     * 用户id
     */
    private String id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 分数
     */
    private String score;

    /**
     * 是否通过
     */
    private Double pass;

}

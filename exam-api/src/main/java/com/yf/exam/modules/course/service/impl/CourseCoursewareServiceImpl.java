package com.yf.exam.modules.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.modules.course.dto.request.CoursewareInfoReqDTO;
import com.yf.exam.modules.course.entity.CourseCourseware;
import com.yf.exam.modules.course.mapper.CourseCoursewareMapper;
import com.yf.exam.modules.course.service.CourseCoursewareService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName CourseCoursewareServiceImpl
 * @Description 课程课件业务实现类
 * @Author BlueAutumn
 * @Date 2021/8/23 9:53
 * @Version 1.0
 */
@Service
public class CourseCoursewareServiceImpl extends ServiceImpl<CourseCoursewareMapper, CourseCourseware> implements CourseCoursewareService {

    @Override
    public void saveAll(String courseId, List<CoursewareInfoReqDTO> coursewareInfoReqDTOS) {

        // 先删除原有的课件
        QueryWrapper<CourseCourseware> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(CourseCourseware::getCourseId, courseId);
        this.remove(wrapper);

        // 再将得到的数据添加进去
        List<CourseCourseware> list = new ArrayList<>();

        for (CoursewareInfoReqDTO coursewareInfo : coursewareInfoReqDTOS) {
            CourseCourseware courseCourseware = new CourseCourseware();
            courseCourseware.setCourseId(courseId);
            courseCourseware.setCoursewareId(coursewareInfo.getCoursewareId());
            list.add(courseCourseware);
        }

        this.saveBatch(list);
    }
}

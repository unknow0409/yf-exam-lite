package com.yf.exam.modules.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.exam.modules.course.entity.CoursewareRecord;

/**
 * @author lgy
 * @title :CoursewareRecordMapper
 * @projectName mylite
 * @Data 2021/8/27 17:30
 */
public interface CoursewareRecordMapper  extends BaseMapper<CoursewareRecord> {
}

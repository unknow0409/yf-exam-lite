package com.yf.exam.modules.exam.dto.request;

import com.yf.exam.modules.exam.dto.ExamDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel(value="考试搜索查询请求类", description="考试搜索查询请求类")
public class ExamPagingReqDTO extends ExamDTO {


    @ApiModelProperty(value = "考试部门列表", required=true)
    private ArrayList<String> departIds;


}

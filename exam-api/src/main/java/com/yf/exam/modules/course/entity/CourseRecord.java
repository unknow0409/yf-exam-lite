package com.yf.exam.modules.course.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName CourseRecord
 * @Description 用户学习总课时统计表
 * @Author Mr.Fu
 * @Date 2021-09-16 13:56:03
 * @Version 1.0
 */
@Data
@TableName("el_course_record_time")
public class CourseRecord extends Model<CourseRecord> {
    /**
     * ID
     */
    @TableId(value = "ID", type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 用户ID
     */
    @TableField("user_id")
    private String userId;
    /**
     * 课程ID
     */
    @TableField("course_id")
    private String courseId;
    /**
     * 使用时长(精确到秒--存储)
     *
     */
    @TableField("course_employ_time")
    private int courseEmployTime;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField("change_time")
    private Date changeTime;


}

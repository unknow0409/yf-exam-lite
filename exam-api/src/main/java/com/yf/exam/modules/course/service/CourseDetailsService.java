package com.yf.exam.modules.course.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.course.dto.CourseDetailsDTO;
import com.yf.exam.modules.course.dto.request.CourseDetailsReqDTO;
import com.yf.exam.modules.course.entity.CourseDetails;

/**
 * @author lgy
 * @title :CourseDetailsService
 * @projectName mylite
 * @Data 2021/8/20 14:34
 */
public interface CourseDetailsService extends IService<CourseDetails> {

    IPage<CourseDetailsDTO> courseDetails(PagingReqDTO<CourseDetailsReqDTO> reqDTO);
}

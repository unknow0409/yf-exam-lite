package com.yf.exam.modules.paper.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.paper.entity.PaperUserState;

import java.util.List;

/**
 * <p>
 * 未答试卷业务类
 * </p>
 */
public interface PaperUserStateService extends IService<PaperUserState> {

    /**
     * 寻找未答试卷
     * @param userID
     * @return
     */
    List<PaperUserState> list(String userID);
}

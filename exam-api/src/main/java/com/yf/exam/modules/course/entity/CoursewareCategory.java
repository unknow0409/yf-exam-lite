package com.yf.exam.modules.course.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
 * @ClassName CoursewareCategory
 * @Description 课件类型实体类
 * @Author BlueAutumn
 * @Date 2021/8/26 17:35
 * @Version 1.0
 */
@Data
@TableName("el_courseware_category")
public class CoursewareCategory extends Model<CoursewareCategory> {

    private static final long serialVersionUID = 2000877973614968869L;

    /**
     * 主键ID
     */
    @TableId(value = "ID", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 课件类型名称
     */
    private String name;

}

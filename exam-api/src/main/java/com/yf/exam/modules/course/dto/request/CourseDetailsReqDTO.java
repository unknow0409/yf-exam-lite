package com.yf.exam.modules.course.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author lgy
 * @title :CourseDetailsReqDTO
 * @projectName mylite
 * @Data 2021/8/20 15:02
 */
@Data
@ApiModel(value = "课程详情请求类", description = "课程详情请求类")
public class CourseDetailsReqDTO {

    @ApiModelProperty(value = "用户姓名")
    private String userName;

    @ApiModelProperty(value = "课程名")
    private String courseName;
}

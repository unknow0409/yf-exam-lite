package com.yf.exam.modules.exam.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.modules.exam.dto.ExamDTO;
import com.yf.exam.modules.exam.entity.Exam;
import com.yf.exam.modules.exam.mapper.ExamSearchMapper;
import com.yf.exam.modules.exam.service.ExamSearchService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lgy
 * @Data2021/8/10 22:43
 */
@Service
public class ExamSearchServiceImpl extends ServiceImpl<ExamSearchMapper, Exam> implements ExamSearchService {


    @Override
    public List<String> title(ExamDTO query) {
        List<ExamDTO> title = baseMapper.title(query);
        List<String> str = new ArrayList<>();
        for (ExamDTO exam : title) {
            str.add(exam.getTitle());
        }
        return str;
    }


}

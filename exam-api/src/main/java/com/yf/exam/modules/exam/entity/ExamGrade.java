package com.yf.exam.modules.exam.entity;

import lombok.Data;

import java.sql.Timestamp;

/**
 * <p>
 * 考试评分实体类
 * </p>
 */
@Data
public class ExamGrade {
    /**
     * 考试试卷id
     */
    private String paperId;

    /**
     * 考试人名字
     */
    private String realName;

    /**
     * 考试名字
     */
    private String title;

    /**
     * 当前总分数
     */
    private int userScore;

    /**
     * 单选题得分
     */
    private int radioScore;

    /**
     * 多选题得分
     */
    private int multiScore;

    /**
     * 判断题得分
     */
    private int judgeScore;

    /**
     * 简答题得分
     */
    private int answerScore;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 批改状态
     */
    private  int reviewCount;

    /**
     * 答题数
     */
    private int answerCount;

    /**
     * 试卷状态
     */
    private int state;
}

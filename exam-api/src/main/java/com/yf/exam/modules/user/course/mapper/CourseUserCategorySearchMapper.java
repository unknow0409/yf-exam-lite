package com.yf.exam.modules.user.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.exam.modules.user.course.dto.CourseDTO;
import com.yf.exam.modules.user.course.entity.Course;

import java.util.List;

/**
 * @author lgy
 * @title :CourseUserCategorySearchMapper
 * @projectName mylite
 * @Data 2021/9/14 9:49
 */
public interface CourseUserCategorySearchMapper  extends BaseMapper<Course> {

    List<String> courseUserCategorySearch(CourseDTO query);
}

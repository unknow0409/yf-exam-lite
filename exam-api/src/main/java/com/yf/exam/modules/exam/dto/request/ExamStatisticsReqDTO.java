package com.yf.exam.modules.exam.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author lgy
 * @Data2021/8/12 16:39
 */
@Data
@ApiModel(value="考试查询请求类", description="考试查询请求类")
public class ExamStatisticsReqDTO {

    @ApiModelProperty(value = "是否分科")
    private boolean subject;

    @ApiModelProperty(value = "开始时间")
    private String startTime;

    @ApiModelProperty(value = "结束时间")
    private  String endTime;

    @ApiModelProperty(value = "考试名称")
    private String[]  examName;




}

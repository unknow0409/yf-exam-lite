package com.yf.exam.modules.course.dto.response;

import com.yf.exam.modules.catalog.entity.CatalogSon;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName CourseCategoryRespDTO
 * @Description 课程类目树数据响应类
 * @Author BlueAutumn
 * @Date 2021/8/26 17:15
 * @Version 1.0
 */
@Data
@ApiModel(value = "课程类目树", description = "课程类目树")
public class CourseCategoryTreeRespDTO extends CatalogSon {

    private static final long serialVersionUID = -7785091059971137990L;

    @ApiModelProperty(value = "子列表", required = true)
    private List<CourseCategoryTreeRespDTO> children;

}

package com.yf.exam.modules.qa.service.impl;

import java.util.Date;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.modules.course.entity.CatalogueCourseware;
import com.yf.exam.modules.course.mapper.CatalogueCoursewareMapper;
import com.yf.exam.modules.sys.user.dto.response.SysUserLoginDTO;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.qa.dto.QaDTO;
import com.yf.exam.modules.qa.entity.Qa;
import com.yf.exam.modules.qa.mapper.QaMapper;
import com.yf.exam.modules.qa.service.QaService;

@Service
public class QaServiceImpl extends ServiceImpl<QaMapper, Qa> implements QaService{

	@Override
	public IPage<QaDTO> getQuestionAnswer(PagingReqDTO<QaDTO> qaDTO) {
		//创建分页对象
        Page page = new Page(qaDTO.getCurrent(), qaDTO.getSize());
        return baseMapper.getQuestionAnswer(page, qaDTO.getParams());

		
	}

	@Override
	public void save(QaDTO qaDTO) {
		Qa qa=new Qa();
		qa.setCourseId(qaDTO.getCourseId());
		qa.setCoursewareId(qaDTO.getCoursewareId());
		qa.setTitle(qaDTO.getTitle());
		qa.setContent(qaDTO.getContent());
		qa.setCreationTime(new Date());
		qa.setState(2);
		//获取当前用户
		Subject subject= SecurityUtils.getSubject();
		SysUserLoginDTO user = (SysUserLoginDTO) subject.getPrincipal();
		qa.setQuestionPeople(user.getUserName());
		baseMapper.insert(qa);
		
	}

	@Override
	public void updata(QaDTO qaDTO) {
		Qa qa=new Qa();
		qa.setAnswer(qaDTO.getAnswer());
		qa.setTitle(qaDTO.getTitle());
		qa.setId(qaDTO.getId());
		qa.setState(1);
		baseMapper.updateById(qa);
		
	}

}

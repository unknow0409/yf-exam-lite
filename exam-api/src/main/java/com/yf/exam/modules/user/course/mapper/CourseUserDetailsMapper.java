package com.yf.exam.modules.user.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yf.exam.modules.user.course.dto.CourseDTO;
import com.yf.exam.modules.user.course.dto.request.CourseUserDetailsReqDTO;
import com.yf.exam.modules.user.course.entity.Course;
import org.apache.ibatis.annotations.Param;

/**
 * @author lgy
 * @title :CoursUsereDetails
 * @projectName mylite
 * @Data 2021/8/19 14:59
 */
public interface CourseUserDetailsMapper extends BaseMapper<Course> {

    IPage<CourseDTO> courseDetails(Page page, @Param("query") CourseUserDetailsReqDTO query);
}

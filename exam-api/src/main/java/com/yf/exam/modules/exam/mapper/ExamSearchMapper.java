package com.yf.exam.modules.exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.exam.modules.exam.dto.ExamDTO;
import com.yf.exam.modules.exam.entity.Exam;

import java.util.List;

/**
 * @author lgy
 * @Data2021/8/10 17:34
 */
public interface ExamSearchMapper extends BaseMapper<Exam> {

    /**
     * 按考试名称查找
     */
    List<ExamDTO> title(ExamDTO query);
}

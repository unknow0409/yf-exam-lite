package com.yf.exam.modules.exam.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.exam.dto.ExamStatisticsDTO;
import com.yf.exam.modules.exam.dto.request.ExamStatisticsReqDTO;
import com.yf.exam.modules.exam.entity.ExamStatistics;


/**
 * @author lgy
 * @Data2021/8/11 15:52
 */
public interface ExamStatisticsService extends IService<ExamStatistics> {

    IPage<ExamStatisticsDTO> statistics(PagingReqDTO<ExamStatisticsReqDTO> reqDTO);
}

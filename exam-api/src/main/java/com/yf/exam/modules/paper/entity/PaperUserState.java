package com.yf.exam.modules.paper.entity;

import lombok.Data;

@Data
public class PaperUserState {

    /**
     * 试卷id
     */
    private String paperId;

    /**
     * 试卷标题
     */
    private String title;

    /**
     * 试卷题数
     */
    private int quCount;

    /**
     * 已做题数
     */
    private int rightCount;
}

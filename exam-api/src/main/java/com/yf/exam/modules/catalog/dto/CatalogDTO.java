package com.yf.exam.modules.catalog.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName CatalogDTO
 * @Description 分类字典数据传输类
 * @Author BlueAutumn
 * @Date 2021/9/22 9:06
 * @Version 1.0
 */
@Data
@ApiModel(value = "分类字典", description = "分类字典")
public class CatalogDTO implements Serializable {

    private static final long serialVersionUID = -9102401449698288578L;

    @ApiModelProperty(value = "主键ID", required = true)
    private String id;

    @ApiModelProperty(value = "分类字典名称", required = true)
    @TableField(value = "catalog_name")
    private String catalogName;

    @ApiModelProperty(value = "分类字典编码", required = true)
    @TableField(value = "catalog_code")
    private String catalogCode;

    @ApiModelProperty(value = "分类字典描述", required = true)
    private String description;

    @ApiModelProperty(value = "创建时间", required = true)
    private Date createTime;

    @ApiModelProperty(value = "更新时间", required = true)
    private Date updateTime;

}

package com.yf.exam.modules.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.core.utils.BeanMapper;
import com.yf.exam.modules.course.dto.CatalogueCoursewareDTO;
import com.yf.exam.modules.course.dto.request.CatalogueInfosReqDTO;
import com.yf.exam.modules.course.dto.request.CatalogueReqDTO;
import com.yf.exam.modules.course.dto.request.CoursewareInfoReqDTO;
import com.yf.exam.modules.course.entity.CatalogueCourseware;
import com.yf.exam.modules.course.mapper.CatalogueCoursewareMapper;
import com.yf.exam.modules.course.mapper.CourseCatalogueMapper;
import com.yf.exam.modules.course.service.CatalogueCoursewareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName CatalogueCoursewareServiceImpl
 * @Description 目录课件业务实现类
 * @Author BlueAutumn
 * @Date 2021/8/20 20:02
 * @Version 1.0
 */
@Service
public class CatalogueCoursewareServiceImpl extends ServiceImpl<CatalogueCoursewareMapper, CatalogueCourseware> implements CatalogueCoursewareService {

    @Autowired
    public CatalogueCoursewareMapper baseMapper;

    @Autowired
    public CourseCatalogueMapper courseCatalogueMapper;

    @Override
    public void saveAll(String courseId, List<CatalogueInfosReqDTO> coursewareInfoList) {

        // 获取目录ID
        List<String> catalogueIds = courseCatalogueMapper.getIdListByCourseId(courseId);

        // 先删除原有的所有相关目录下的课件
        for (String catalogueId : catalogueIds) {
            QueryWrapper<CatalogueCourseware> wrapper = new QueryWrapper<>();
            wrapper.lambda().eq(CatalogueCourseware::getCatalogueId, catalogueId);
            this.remove(wrapper);
        }


        // 再将得到的数据添加进去
        List<CatalogueCourseware> list = new ArrayList<>();

        for (CatalogueInfosReqDTO catalogueInfo : coursewareInfoList) {
            for (CoursewareInfoReqDTO reqDTO : catalogueInfo.getCoursewareInfoList()) {
                CatalogueCourseware catalogueCourseware = new CatalogueCourseware();
                BeanMapper.copy(reqDTO, catalogueCourseware);
                catalogueCourseware.setCatalogueId(courseCatalogueMapper.getIdByCatalogueNameAndCourseId(courseId, catalogueInfo.getCatalogueName()));
                list.add(catalogueCourseware);
            }
        }
        this.saveBatch(list);

        // 删除剩下的没有用的
        // 先查出现有的目录ID表，再删除没有的
        List<String> catalogueIdList = courseCatalogueMapper.idList();
        baseMapper.removeUselessByCatalogueIds(catalogueIdList);
    }

    @Override
    public List<CatalogueCoursewareDTO> listByCatalogueId(CatalogueReqDTO reqDTO) {
        return baseMapper.getListByCatalogueId(reqDTO.getCatalogueId());
    }

}

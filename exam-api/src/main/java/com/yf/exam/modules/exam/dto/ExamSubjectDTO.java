package com.yf.exam.modules.exam.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 考试题目实体类
 * </p>
 */
@Data
@ApiModel(value = "试卷题目",description = "试卷题目")
public class ExamSubjectDTO {

    @ApiModelProperty(value = "试卷id")
    private String paperId;

    @ApiModelProperty(value = "问题id")
    private String quId;

    @ApiModelProperty(value = "题目类型")
    private int quType;

    @ApiModelProperty(value = "实际成绩")
    private int actualScore;

    @ApiModelProperty(value = "难度")
    private int level;

    @ApiModelProperty(value = "题目")
    private String content;

    @ApiModelProperty(value = "是否批改")
    private int isReview;

    @ApiModelProperty(value = "是否正确")
    private int isRight;
}

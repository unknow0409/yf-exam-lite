package com.yf.exam.modules.course.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.course.dto.CoursewareDTO;
import com.yf.exam.modules.course.dto.request.CoursewareDetailsReqDTO;
import com.yf.exam.modules.course.entity.Courseware;
import com.yf.exam.modules.course.mapper.CoursewareDetailsMapper;
import com.yf.exam.modules.course.service.CoursewareDetailsService;
import org.springframework.stereotype.Service;

/**
 * @author lgy
 * @title :CoursewareDetailsServiceImpl
 * @projectName mylite
 * @Data 2021/8/26 14:59
 */
@Service
public class CoursewareDetailsServiceImpl extends ServiceImpl<CoursewareDetailsMapper, Courseware> implements CoursewareDetailsService {

    @Override
    public IPage<CoursewareDTO> coursewareDetails(PagingReqDTO<CoursewareDetailsReqDTO> reqDTO){
        //创建分页对象
        Page page = new Page(reqDTO.getCurrent(), reqDTO.getSize());

        IPage<CoursewareDTO> coursewareDate = baseMapper.coursewareDetails(page, reqDTO.getParams());

        return coursewareDate;
    }

}

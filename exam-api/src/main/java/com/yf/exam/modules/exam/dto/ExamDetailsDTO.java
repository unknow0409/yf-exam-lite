package com.yf.exam.modules.exam.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author lgy
 * @title :ExamDetailsDTO
 * @projectName mylite
 * @Data 2021/8/16 15:37
 */
@Data
@ApiModel(value = "考试数据详情", description = "考试数据详情")
public class ExamDetailsDTO {

    @ApiModelProperty(value = "试卷id")
    private String paperId;

    @ApiModelProperty(value = "考试名称")
    private String title;

    @ApiModelProperty(value = "考试日期")
    private String date;

    @ApiModelProperty(value = "考试时间")
    private String time;

    @ApiModelProperty(value = "用户id")
    private String id;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "分数")
    private String score;

    @ApiModelProperty(value = "合格")
    private double pass;
}

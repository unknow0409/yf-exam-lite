package com.yf.exam.modules.course.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName CourseDepartDTO
 * @Description 课程部门数据传输类
 * @Author BlueAutumn
 * @Date 2021/8/20 9:40
 * @Version 1.0
 */
@Data
@ApiModel(value = "课程部门", description = "课程部门")
public class CourseDepartDTO implements Serializable {

    private static final long serialVersionUID = 2881485860324744845L;

    @ApiModelProperty(value = "ID", required = true)
    private String id;

    @ApiModelProperty(value = "课程ID", required = true)
    private String courseId;

    @ApiModelProperty(value = "部门ID", required = true)
    private String departId;

}

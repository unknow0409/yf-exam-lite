package com.yf.exam.modules.catalog.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.core.utils.BeanMapper;
import com.yf.exam.core.utils.SpringUtils;
import com.yf.exam.core.utils.StringUtils;
import com.yf.exam.modules.catalog.dto.request.CatalogSonDataReqDTO;
import com.yf.exam.modules.catalog.dto.response.CatalogSonRespDTO;
import com.yf.exam.modules.catalog.entity.Catalog;
import com.yf.exam.modules.catalog.entity.CatalogSon;
import com.yf.exam.modules.catalog.mapper.CatalogSonMapper;
import com.yf.exam.modules.catalog.service.CatalogService;
import com.yf.exam.modules.catalog.service.CatalogSonService;
import com.yf.exam.modules.course.dto.response.CourseCategoryTreeRespDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @ClassName CatalogSonServiceImpl
 * @Description 分类子表
 * @Author Mr.Fu
 * @Date 2021-09-22 09:13:50
 * @Version 1.0
 */
@Service
public class CatalogSonServiceImpl extends ServiceImpl<CatalogSonMapper, CatalogSon> implements CatalogSonService {

    @Autowired
    private CatalogService catalogService;

    @Override
    public List<CourseCategoryTreeRespDTO> findTree(String code) {

        List<CourseCategoryTreeRespDTO> respDTOList = baseMapper.findFather(code);
        return findTrees(respDTOList);
    }

    private List<CourseCategoryTreeRespDTO> findTrees(List<CourseCategoryTreeRespDTO> respDTOList) {

        if (respDTOList.size() > 0) {
            for (CourseCategoryTreeRespDTO respDTO : respDTOList) {
                List<CourseCategoryTreeRespDTO> children = baseMapper.findChildren(respDTO.getId());
                respDTO.setChildren(findTrees(children));
            }
        }
        return respDTOList;
    }

    /**
     * 删除
     */
    public void delete(CatalogSon catalogSon) {
        this.recursionDelete(catalogSon);
        catalogSon.deleteById();
    }

    /**
     * 查找
     */
    public List<CatalogSonRespDTO> findAllData(CatalogSonDataReqDTO reqDTO) {
        if (StringUtils.isBlank(reqDTO.getCatalogId())){
            if (!StringUtils.isBlank(reqDTO.getCatalogCode())){
                QueryWrapper<Catalog> wrapper =new QueryWrapper<>();
                wrapper.lambda().eq(Catalog::getCatalogCode, reqDTO.getCatalogCode());
                reqDTO.setCatalogId(catalogService.getOne(wrapper).getId());
            }else {
                return null;
            }
        }
        return this.recursionFind(reqDTO);
    }

    /**
     * 递归删除方法
     */
    public void recursionDelete(CatalogSon catalogSon) {
        QueryWrapper<CatalogSon> wrapper = new QueryWrapper<>();
        wrapper.lambda().select(CatalogSon::getId).eq(CatalogSon::getParentId,catalogSon.getId());
        List<CatalogSon> catalogSons = catalogSon.selectList(wrapper);
        if (!catalogSons.isEmpty()) {
            List<String> ids = new ArrayList<>();
            for (CatalogSon son : catalogSons) {
                ids.add(son.getId());
                this.recursionDelete(son);
            }
            ((CatalogSonMapper)this.baseMapper).deleteBatchIds(ids);
        }

    }

    /**
     * 递归查找方法
     */
    public List<CatalogSonRespDTO> recursionFind(CatalogSonDataReqDTO reqDTO) {
        List<CatalogSonRespDTO> list = new ArrayList<>();
        List<CatalogSon> catalogSonsOne = baseMapper.findAllCatalogSon(reqDTO);
        if (!catalogSonsOne.isEmpty()) {
            for (CatalogSon son : catalogSonsOne) {
                CatalogSonRespDTO catalogSonRespDTO = new CatalogSonRespDTO();
                BeanMapper.copy(son, catalogSonRespDTO);
                reqDTO.setCatalogId(son.getId());
                List<CatalogSonRespDTO> listOne = this.recursionFind(reqDTO);
                catalogSonRespDTO.setSonList(listOne);
                list.add(catalogSonRespDTO);
            }
        }
        return list;
    }

}

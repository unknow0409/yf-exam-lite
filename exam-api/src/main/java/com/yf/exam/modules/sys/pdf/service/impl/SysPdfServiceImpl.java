package com.yf.exam.modules.sys.pdf.service.impl;

import com.yf.exam.core.utils.excel.ExportExcel;
import com.yf.exam.modules.sys.pdf.service.SysPdfService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;

@Service
public class SysPdfServiceImpl implements SysPdfService {
    private static Logger log = LoggerFactory.getLogger(ExportExcel.class);

    @Override
    public byte[] previewPdf(String filePath, HttpServletRequest request, HttpServletResponse response) {
        File file = new File(filePath);
        byte[] data = new byte[0];
        try {
            FileInputStream inputStream = new FileInputStream(file);
            data = new byte[inputStream.available()];
            inputStream.read(data);
            response.getOutputStream().write(data);
            inputStream.close();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return data;
    }
}

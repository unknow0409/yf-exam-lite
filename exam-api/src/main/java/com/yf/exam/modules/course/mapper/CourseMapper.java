package com.yf.exam.modules.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yf.exam.modules.course.dto.CourseDTO;
import com.yf.exam.modules.course.dto.request.CoursePagReqDTO;
import com.yf.exam.modules.course.dto.request.CourseUserIDReqDTO;
import com.yf.exam.modules.course.dto.response.CoursewareVideoInfoRespDTO;
import com.yf.exam.modules.course.entity.Course;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *@InterfaceName CourseMapper
 *@Description 课程Mapper
 *@Author BlueAutumn
 *@Date 2021/8/19 10:55
 *@Version 1.0
 */
public interface CourseMapper extends BaseMapper<Course> {

    /**
     * 获取课程的信息（使用时间，剩余时间）
     * @param reqDTO
     * @return
     */
    CoursewareVideoInfoRespDTO findCourseTimeOneInfo(CourseUserIDReqDTO reqDTO);

    IPage<CourseDTO> paging(Page page, @Param("query") CoursePagReqDTO query);

    List<String> getDepartIdsByCourseId(@Param("courseId") String courseId);

    List<String> getCatalogueIdsByCourseId(@Param("courseId") String courseId);


}
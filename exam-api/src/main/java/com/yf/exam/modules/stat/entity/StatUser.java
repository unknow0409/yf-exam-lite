package com.yf.exam.modules.stat.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName StatUser
 * @Description 用户统计实体类
 * @Author BlueAutumn
 * @Date 2021/8/16 16:19
 * @Version 1.0
 */
@Data
public class StatUser extends Model<StatUser> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    //@TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 统计时间
     */
    @TableField("stat_time")
    private Date statTime;

    /**
     * 活跃用户数
     */
    private Integer activeUser;

    /**
     * 新增用户数
     */
    private Integer newUser;

    /**
     * 总用户数
     */
    private Integer totalUser;

}

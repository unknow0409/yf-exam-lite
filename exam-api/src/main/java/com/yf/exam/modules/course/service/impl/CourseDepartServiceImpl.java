package com.yf.exam.modules.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.core.exception.ServiceException;
import com.yf.exam.modules.course.entity.CourseDepart;
import com.yf.exam.modules.course.mapper.CourseDepartMapper;
import com.yf.exam.modules.course.service.CourseDepartService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName CourseDepartServiceImpl
 * @Description 课程部门业务实现类
 * @Author BlueAutumn
 * @Date 2021/8/20 9:56
 * @Version 1.0
 */
@Service
public class CourseDepartServiceImpl extends ServiceImpl<CourseDepartMapper, CourseDepart> implements CourseDepartService {

    @Override
    public void saveAll(String courseId, List<String> departIds) {

        // 先删除
        QueryWrapper<CourseDepart> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(CourseDepart::getCourseId, courseId);
        this.remove(wrapper);

        // 再增加
        if (CollectionUtils.isEmpty(departIds)) {
            throw new ServiceException(1, "请至少选择选择一个部门！！");
        }
        List<CourseDepart> list = new ArrayList<>();

        for (String id : departIds) {
            CourseDepart depart = new CourseDepart();
            depart.setDepartId(id);
            depart.setCourseId(courseId);
            list.add(depart);
        }

        this.saveBatch(list);
    }
}

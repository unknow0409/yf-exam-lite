package com.yf.exam.modules.course.dto.response;

import com.yf.exam.modules.course.dto.CoursewareDTO;
import com.yf.exam.modules.course.entity.Courseware;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName CoursewareCatalogueInfoRespDTO
 * @Description 课件的使用时间响应类
 * @Author Mr.Fu
 * @Date 2021-08-20 14:16:03
 * @Version 1.0
 */
@Data
public class CoursewareUseTimeRespDTO extends CoursewareDTO {

    @ApiModelProperty(value = "课件的使用时间", required = true)
    private Integer coursewareEmployTime;

    @ApiModelProperty(value = "课件的顺序", required = true)
    private Integer sorting;

    @ApiModelProperty(value = "课件积分", required = true)
    private String coursewareIntegral;

    @ApiModelProperty(value = "课件学习时长", required = true)
    private Integer coursewareTime;

}

package com.yf.exam.modules.course.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.exam.modules.course.entity.CourseRecord;
import com.yf.exam.modules.course.mapper.CourseRecordMapper;
import com.yf.exam.modules.course.service.CourseRecordService;
import org.springframework.stereotype.Service;

/**
 * @ClassName CourseRecordServiceImpl
 * @Description 用户学习总课时统计
 * @Author Mr.Fu
 * @Date 2021-09-16 14:21:12
 * @Version 1.0
 */
@Service
public class CourseRecordServiceImpl  extends ServiceImpl<CourseRecordMapper, CourseRecord> implements CourseRecordService {

}

package com.yf.exam.modules.course.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yf.exam.core.api.ApiRest;
import com.yf.exam.core.api.controller.BaseController;
import com.yf.exam.core.api.dto.BaseIdReqDTO;
import com.yf.exam.core.api.dto.BaseIdsReqDTO;
import com.yf.exam.core.api.dto.BaseQueryReqDTO;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.catalog.service.CatalogSonService;
import com.yf.exam.modules.course.dto.*;
import com.yf.exam.modules.course.dto.request.*;
import com.yf.exam.modules.course.dto.response.CourseCategoryTreeRespDTO;
import com.yf.exam.modules.course.dto.response.CourseInfoRespDTO;
import com.yf.exam.modules.course.dto.response.CoursewareCategoryRespDTO;
import com.yf.exam.modules.course.dto.response.CoursewareVideoInfoRespDTO;
import com.yf.exam.modules.course.entity.CourseRecord;
import com.yf.exam.modules.course.entity.Courseware;
import com.yf.exam.modules.course.entity.CoursewareRecord;
import com.yf.exam.modules.course.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName CourseController
 * @Description 课程控制器
 * @Author BlueAutumn
 * @Date 2021/8/19 10:48
 * @Version 1.0
 */
@Api
@RestController
@RequestMapping("/course/api/course/course")
public class CourseController extends BaseController {

    @Autowired
    private CourseService baseService;

    @Autowired
    private CoursewareService coursewareService;

    @Autowired
    private CatalogueCoursewareService catalogueCoursewareService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private CourseSearchService courseSearchService;

    @Autowired
    private CourseDetailsService courseDetailsService;

    @Autowired
    private CoursewareCategoryService coursewareCategoryService;

    @Autowired
    private CoursewareDetailsService coursewareDetailsService;

    @Autowired
    private CatalogSonService catalogSonService;


    /**
     * 添加或修改
     *
     * @param courseware
     * @return
     */
    @ApiOperation(value = "获取课件链接")
    @RequestMapping(value = "/address", method = {RequestMethod.POST})
    public ApiRest<String> findAddress(@RequestBody Courseware courseware) {
        String addressById = coursewareService.findAddressById(courseware.getId());
        return super.success(addressById);
    }

    /**
     * 获取课程的详细信息及目录列表
     *
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "获取课件的详细信息及目录列表")
    @RequestMapping(value = "/courseInfo/catalogue", method = {RequestMethod.POST})
    public ApiRest<CoursewareVideoInfoRespDTO> findCourseAndCatalogue(@RequestBody CourseUserIDReqDTO reqDTO) {
        CoursewareVideoInfoRespDTO respDTO = courseService.findCourseAndCatalogue(reqDTO);
        return super.success(respDTO);
    }
    @ApiOperation(value = "更新用户学习总课时统计表")
    @RequestMapping(value = "/courseEmployTime/update", method = {RequestMethod.POST})
    public ApiRest<CourseRecordDTO> updateCourseEmployTime(@RequestBody CourseRecord courseRecord){
        UpdateWrapper<CourseRecord> wrapper=new UpdateWrapper<>();
        wrapper.lambda().eq(CourseRecord::getUserId,courseRecord.getUserId())
                .eq(CourseRecord::getCourseId,courseRecord.getCourseId());
        courseRecord.update(wrapper);
        return super.success();
    }
    @ApiOperation(value = "更新用户学习课件时长统计表")
    @RequestMapping(value = "/coursewareEmployTime/update", method = {RequestMethod.POST})
    public ApiRest<CourseRecordDTO> updateCoursewareEmployTime(@RequestBody CoursewareRecord coursewareRecord){
        UpdateWrapper<CoursewareRecord> wrapper=new UpdateWrapper<>();
        wrapper.lambda().eq(CoursewareRecord::getCoursewareId,coursewareRecord.getCoursewareId())
                .eq(CoursewareRecord::getUserId,coursewareRecord.getUserId())
                .eq(CoursewareRecord::getCourseId,coursewareRecord.getCourseId());
        coursewareRecord.update(wrapper);
        return super.success();
    }

    /**
     * 课程名查询
     *
     * @return
     */
    @ApiOperation(value = "课程名查询")
    @RequestMapping(value = "/courseSearch", method = {RequestMethod.POST})
    public ApiRest<List<String>> courseSearch(@RequestBody CourseDTO query) {

        List<String> searchData = courseSearchService.courseSearch(query);

        return super.success(searchData);
    }


    /**
     * 课程详情
     * @return
     */
    @ApiOperation(value = "课程详情")
    @RequestMapping(value = "/courseDetails", method = {RequestMethod.POST})
    public ApiRest<IPage<CourseDetailsDTO>> courseDetail(@RequestBody PagingReqDTO<CourseDetailsReqDTO> query) {

        IPage<CourseDetailsDTO> detailsData = courseDetailsService.courseDetails(query);

        return super.success(detailsData);
    }



    /**
     * 添加或修改
     *
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "添加或修改")
    @RequestMapping(value = "/save", method = {RequestMethod.POST})
    public ApiRest save(@RequestBody BaseSaveReqDTO<CourseSaveReqDTO> reqDTO) {

        // 复制参数
        baseService.save(reqDTO.getParams());
        return super.success();
    }

    /**
     * 分页查找以及筛选课程
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "分页查找及筛选")
    @RequestMapping(value = "/paging", method = {RequestMethod.POST})
    public ApiRest<IPage<CourseDTO>> paging(@RequestBody PagingReqDTO<CoursePagReqDTO> reqDTO) {

        // 分页查询
        IPage<CourseDTO> page = baseService.paging(reqDTO);

        return super.success(page);
    }

    /**
     * 批量删除课程（递归删除旗下的的东西）
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "批量删除课程（递归删除旗下的的东西）")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public ApiRest edit(@RequestBody BaseIdsReqDTO reqDTO) {
        // 根据ID批量删除
        baseService.removeBatchByIds(reqDTO.getIds());
        return super.success();
    }

    /**
     * 根据目录ID查询课件
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "查询课件")
    @RequestMapping(value = "/search_courseware", method = {RequestMethod.POST})
    public ApiRest listOfCourseware(@RequestBody CatalogueReqDTO reqDTO) {
        // 根据目录ID查询课件
        List<CatalogueCoursewareDTO> dto = catalogueCoursewareService.listByCatalogueId(reqDTO);
        return super.success(dto);
    }

    /**
     * 课程详情修改
     * @param respDTO
     * @return
     */
    @ApiOperation(value = "课程详情")
    @RequestMapping(value = "/search_course", method = {RequestMethod.POST})
    public ApiRest<CourseInfoRespDTO> findCourse(@RequestBody BaseIdReqDTO respDTO) {
        // 根据ID查询课程
        CourseInfoRespDTO dto = baseService.getInfoById(respDTO.getId());

        return super.success(dto);
    }

    /**
     * 查询课件类型信息
     * @return
     */
    @ApiOperation(value = "课件类型信息")
    @RequestMapping(value = "/courseware/category-detail", method = {RequestMethod.POST})
    public ApiRest<List<String>> findCoursewareCategory() {

        CoursewareCategoryRespDTO dto = coursewareCategoryService.listName();

        return super.success(dto.getCoursewareCategoryList());
    }

    /**
     * 查询课程类目信息
     * @return
     */
    @RequestMapping(value = "/category-detail", method = {RequestMethod.POST})
    public ApiRest<List<CourseCategoryTreeRespDTO>> courseCategoryTree(@RequestBody BaseQueryReqDTO reqDTO) {

        List<CourseCategoryTreeRespDTO> dtoTree = catalogSonService.findTree(reqDTO.getQ());
        return super.success(dtoTree);
    }

    /**
     * 课件详情
     * @param query
     */
        @ApiOperation(value = "课件详情")
        @RequestMapping(value = "/search_coursewareDetails", method = {RequestMethod.POST})
        public ApiRest<IPage<CoursewareDTO>> coursewareDetail(@RequestBody PagingReqDTO<CoursewareDetailsReqDTO> query) {
            // 根据课件名称，课件类型查询课件
            IPage<CoursewareDTO> coursewareData = coursewareDetailsService.coursewareDetails(query);

            return super.success(coursewareData);
        }

    /**
     * 课件添加或修改
     *
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "添加或修改")
    @RequestMapping(value = "/courseware/save", method = {RequestMethod.POST})
    public ApiRest coursewareSave(@RequestBody BaseSaveReqDTO<CoursewareDTO> reqDTO){

        // 复制参数
        coursewareService.saveCourseware(reqDTO.getParams());

        return super.success();
    }


    /**
     * 课件批量删除
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "课件批量删除")
    @RequestMapping(value = "/courseware/delete", method = { RequestMethod.POST})
    public ApiRest coursewareEdit(@RequestBody BaseIdsReqDTO reqDTO) {
        //根据ID删除
        coursewareService.removeManyById(reqDTO.getIds());
        return super.success();

    }


    /**
     * 根据课件id，返回课件的实体类
    * @param reqDTO
     * @return
     */
    @ApiOperation(value = "根据课件id，返回课件的实体类")
    @RequestMapping (value = "/courseware/entity", method = { RequestMethod.POST})
    public ApiRest<CoursewareDTO> getInfoById(@RequestBody BaseIdReqDTO reqDTO){

        // 根据ID查询课件
        CoursewareDTO dto = coursewareService.getInfoById(reqDTO.getId());
        return super.success(dto);
    }

}

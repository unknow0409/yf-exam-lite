package com.yf.exam.modules.qa.dto;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="问答", description="问答信息")
public class QaDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "问答ID",required=true)
    private String id;
	
	@ApiModelProperty(value = "所属课程ID",required=true)
	private String courseId;
	
	@ApiModelProperty(value = "所属课件ID",required=true)
	private String coursewareId;
	
	@ApiModelProperty(value = "所属课程名称")
	private String courseName;
	
	@ApiModelProperty(value = "所属课件名称")
	private String coursewareName;
	
	@ApiModelProperty(value = "提问人",required=true)
	private String questionPeople;
	
	@ApiModelProperty(value = "问题标题",required=true)
	private String title;
	
	@ApiModelProperty(value = "创建时间")
	private Date creationTime;
	
	@ApiModelProperty(value = "提问内容",required=true)
	private String content;
	
	@ApiModelProperty(value = "问题答复")
	private String answer;
	
	@ApiModelProperty(value = "回复状态")
	private int state;
	
}

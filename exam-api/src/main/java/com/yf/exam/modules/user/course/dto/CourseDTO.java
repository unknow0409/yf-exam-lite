package com.yf.exam.modules.user.course.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author lgy
 * @title :CourseDTO
 * @projectName mylite
 * @Data 2021/8/19 14:42
 */
@Data
@ApiModel(value = "课程", description = "课程")
public class CourseDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程ID", required = true)
    private String id;

    @ApiModelProperty(value = "课程名称", required = true)
    private String courseName;

    @ApiModelProperty(value = "课程封面地址", required = true)
    private String img;

    @ApiModelProperty(value = "课程简介", required = true)
    private String description;

    @ApiModelProperty(value = "课程分类", required = true)
    private String courseCategory;

    @ApiModelProperty(value = "课时", required = true)
    private Double courseTime;

    @ApiModelProperty(value = "课程创建时间", required = true)
    private Date createTime;

    @ApiModelProperty(value = "课程修改时间", required = true)
    private Date changeTime;

    @ApiModelProperty(value = "课程积分", required = true)
    private Double courseIntegral;

    @ApiModelProperty(value = "课程是否必修，0非必修,1必修", required = true)
    private Integer isNecessary;

    @ApiModelProperty(value = "课程开放权限，0完全开放，1指定用户开放", required = true)
    private Integer openType;

}
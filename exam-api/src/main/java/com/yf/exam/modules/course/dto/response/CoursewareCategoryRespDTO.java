package com.yf.exam.modules.course.dto.response;

import com.yf.exam.modules.course.dto.CoursewareDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName CoursewareCategoryRespDTO
 * @Description 课程类型数据响应类
 * @Author BlueAutumn
 * @Date 2021/8/26 14:40
 * @Version 1.0
 */
@Data
@ApiModel(value = "课程类型", description = "课程类型")
public class CoursewareCategoryRespDTO implements Serializable {

    private static final long serialVersionUID = -8693390202498733130L;

    @ApiModelProperty(value = "课件类型列表", required = true)
    private List<String> coursewareCategoryList;

}

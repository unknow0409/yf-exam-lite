package com.yf.exam.modules.course.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.course.entity.CourseDepart;

import java.util.List;

/**
 *@InterfaceName CourseDepartService
 *@Description 课程部门业务类
 *@Author BlueAutumn
 *@Date 2021/8/20 9:56
 *@Version 1.0
 */
public interface CourseDepartService extends IService<CourseDepart> {

    /**
     * 保存全部部门
     * @param courseId
     * @param departIds
     */
    void saveAll(String courseId, List<String> departIds);
}
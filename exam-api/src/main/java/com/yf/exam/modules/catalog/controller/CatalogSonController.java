package com.yf.exam.modules.catalog.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.yf.exam.core.api.ApiRest;
import com.yf.exam.core.api.controller.BaseController;
import com.yf.exam.modules.catalog.dto.request.CatalogSonDataReqDTO;
import com.yf.exam.modules.catalog.dto.response.CatalogSonRespDTO;
import com.yf.exam.modules.catalog.entity.CatalogSon;
import com.yf.exam.modules.catalog.service.CatalogSonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @ClassName CatalogSon
 * @Description 分类子表
 * @Author Mr.Fu
 * @Date 2021-09-22 08:51:30
 * @Version 1.0
 */
@Api(tags={"分类子表"})
@RestController
@RequestMapping("/exam/api/catalog/son")
public class CatalogSonController extends BaseController {

    @Autowired
    private CatalogSonService catalogSonService;

    /**
     * 增加分类子表数据
     */
    @ApiOperation(value = "添加分类子表数据")
    @RequestMapping(value = "/insert", method = {RequestMethod.POST})
    public ApiRest insert(@RequestBody CatalogSon catalogSon) {
        catalogSon.insert();
        return super.success();
    }

    /**
     * 根据分类ID查找所有的子表数据
     */
    @ApiOperation(value = "获取分类子表数据")
    @RequestMapping(value = "/data", method = {RequestMethod.POST})
    public ApiRest<List<CatalogSonRespDTO>> findData(@RequestBody CatalogSonDataReqDTO reqDTO) {
        List<CatalogSonRespDTO> allData = catalogSonService.findAllData(reqDTO);
        return super.success(allData);
    }

    /**
     * 根据分类子表ID修改分类子表数据
     */
    @ApiOperation(value = "获取分类子表数据")
    @RequestMapping(value = "/updata", method = {RequestMethod.POST})
    public ApiRest updata(@RequestBody CatalogSon catalogSon) {
        catalogSon.updateById();
        return super.success();
    }

    /**
     * 根据分类子表ID删除分类子表数据
     */
    @ApiOperation(value = "删除分类子表数据")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public ApiRest delete(@RequestBody CatalogSon catalogSon) {
        catalogSonService.delete(catalogSon);
        return super.success();
    }





}

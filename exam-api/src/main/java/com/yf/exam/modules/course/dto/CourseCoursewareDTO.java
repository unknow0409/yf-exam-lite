package com.yf.exam.modules.course.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName CourseCoursewareDTO
 * @Description 课程课件数据传输类
 * @Author BlueAutumn
 * @Date 2021/8/23 9:48
 * @Version 1.0
 */
@Data
@ApiModel(value = "课程课件", description = "课程课件")
public class CourseCoursewareDTO implements Serializable {

    private static final long serialVersionUID = 2232488687713354465L;

    private String id;

    @ApiModelProperty(value = "课程ID", required = true)
    private String courseId;

    @ApiModelProperty(value = "课件ID", required = true)
    private String coursewareId;

}

package com.yf.exam.modules.catalog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.catalog.dto.CatalogDTO;
import com.yf.exam.modules.catalog.dto.request.CatalogSearchReqDTO;
import com.yf.exam.modules.catalog.entity.Catalog;

import java.util.List;

/**
 * @ClassName CatalogService
 * @Description 分类字典业务类
 * @Author BlueAutumn
 * @Date 2021/9/22 9:22
 * @Version 1.0
 */
public interface CatalogService extends IService<Catalog> {

    IPage<CatalogDTO> paging(PagingReqDTO<CatalogSearchReqDTO> reqDTO);

    void save(CatalogDTO dto);

    void deleteByIds(List<String> ids);
}

package com.yf.exam.modules.course.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName CourseRecordDTO
 * @Description 用户学习总课时统计表
 * @Author Mr.Fu
 * @Date 2021-09-16 14:05:36
 * @Version 1.0
 */
@Data
@ApiModel(value = "用户学习总课时统计表", description = "用户学习总课时统计表")
public class CourseRecordDTO implements Serializable {

    @ApiModelProperty(value = "ID", required = true)
    private String id;

    @ApiModelProperty(value = "用户ID", required = true)
    private String userId;

    @ApiModelProperty(value = "课程ID", required = true)
    private String courseId;

    @ApiModelProperty(value = "使用时长(精确到秒--存储)", required = true)
    private int courseEmployTime;

    @ApiModelProperty(value = "创建时间", required = true)
    private Date createTime;

    @ApiModelProperty(value = "修改时间", required = true)
    private Date changeTime;
}

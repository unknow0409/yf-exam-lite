package com.yf.exam.modules.exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.modules.exam.dto.request.ExamDetailsReqDTO;
import com.yf.exam.modules.exam.entity.ExamDetails;

import java.util.List;

/**
 * @author lgy
 * @title :ExamDetailsSearchService
 * @projectName mylite
 * @Data 2021/8/16 17:46
 */
public interface ExamDetailsSearchService extends IService<ExamDetails> {

    List<String> detailsSearch(ExamDetailsReqDTO query);
}

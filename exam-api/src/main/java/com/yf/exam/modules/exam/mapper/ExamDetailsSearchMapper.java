package com.yf.exam.modules.exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.exam.modules.exam.dto.request.ExamDetailsReqDTO;
import com.yf.exam.modules.exam.entity.ExamDetails;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lgy
 * @title :ExamDetailsSearchMapper
 * @projectName mylite
 * @Data 2021/8/16 17:47
 */
public interface ExamDetailsSearchMapper extends BaseMapper<ExamDetails> {

    List<String> detailsSearch(@Param("query") ExamDetailsReqDTO query);
}

package com.yf.exam.modules.exam.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.exam.core.api.dto.PagingReqDTO;
import com.yf.exam.modules.exam.dto.ExamDetailsDTO;
import com.yf.exam.modules.exam.dto.request.ExamDetailsReqDTO;
import com.yf.exam.modules.exam.entity.ExamDetails;


/**
 * @author lgy
 * @title :ExamDetailsService
 * @projectName mylite
 * @Data 2021/8/16 10:30
 */
public interface ExamDetailsService extends IService<ExamDetails> {

    IPage<ExamDetailsDTO> details(PagingReqDTO<ExamDetailsReqDTO> reqDTO);
}

package com.yf.exam.Scheduled;

import com.yf.exam.modules.paper.dto.PaperDTO;
import com.yf.exam.modules.paper.mapper.PaperMapper;
import com.yf.exam.modules.paper.service.PaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class PaperScheduled {
    @Autowired
    private PaperService paperService;

    /**
     * 定时更新过期未交卷试卷状态
     * 时间：每小时
     */
    @Scheduled(cron = "0 0 */1 * * *")
    public void updatePaperState(){
        paperService.updatePaperState();
    }
}

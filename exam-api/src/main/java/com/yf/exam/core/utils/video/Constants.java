package com.yf.exam.core.utils.video;
 
/**
 * 常量类,主要设置可执行程序和动态链接库以及转化过程中生成的临时视频文件的位置
 * @author liuyazhuang
 *
 */
public class Constants {

	//ffmpeg存放的路径
	public static final String ffmpegPath = "C:\\java视频格式转换\\ffmpeg\\ffmpeg-20171225\\bin\\ffmpeg.exe";

	//mencoder存放的路径
	public static final String mencoderPath = "C:\\java视频格式转换\\mencoder\\mencoder.exe";

	//通过mencoder转换成的avi存放路径
	public static final String avifilepath = "C:\\java视频格式转换\\ffmpeg\\ffmpeg-20171225\\bin\\temp.avi";

}
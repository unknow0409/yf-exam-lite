package com.yf.exam.core.utils.file;

import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.StreamOpenOfficeDocumentConverter;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * @ClassName Office2Pdf
 * @Description word转pdf
 * @Author Mr.Fu
 * @Date 2021-08-30 11:11:02
 * @Version 1.0
 */
public class Office2Pdf {
    /**
     * office文件转PDF
     * @param file
     * @return
     */
    public static File Office2Pdf(File file,String pdfSavePath, String openOfficeIp){
        Logger logger = LoggerFactory.getLogger(Office2Pdf.class);
        Process p = null;
        OpenOfficeConnection connection = null;
        String rule = "txt|doc|docx|xlsx|xls|pdf|ppt|pptx";
        String fileType = file.getName().substring(file.getName().indexOf(".") + 1);
        if (!fileType.toLowerCase().matches(rule)){
            logger.info("不支持的文件格式{}",fileType);
            return file;
        }
        //判断源文件是否存在
        if (!file.exists()){
            throw new RuntimeException("源文件不存在!");
        }
        //输出文件
        File pdfFile = new File( pdfSavePath + File.separator + file.getName().substring(0,file.getName().lastIndexOf("."))+".pdf");
        // 转换后的pdf文件保存位置
        logger.info("转换后的pdf文件保存位置:{}",pdfFile.getAbsolutePath());

        try {
            logger.debug("PDF开始转换");
            // PDF格式特殊处理
            if ("pdf".equals(fileType)){
                FileUtils.copyFile(file,pdfFile);

            }else{
                // 调用openoffice服务线程
                String command = "C:\\openOffice\\openOffice4\\program\\soffice.exe -headless -accept=\"socket,host="+openOfficeIp+",port=8100;urp;\"";
                p = Runtime.getRuntime().exec(command);
                //创建连接
                connection = new SocketOpenOfficeConnection(openOfficeIp,8100);
                //远程连接OpenOffice服务
                connection.connect();
                //创建文件转换器
                DocumentConverter converter = new StreamOpenOfficeDocumentConverter(connection);
                //开始转换
                converter.convert(file, pdfFile);
            }
            Thread.sleep(1000);
            if (pdfFile.exists()){
                logger.info("文件{}转换成功",file.getAbsolutePath());
            }else {
                logger.info("文件{}转换失败",file.getAbsolutePath());
            }

            if (p != null) {
                // 关闭进程
                p.destroy();
            }
        } catch (Exception e) {

            e.printStackTrace();
            logger.error("office转PDF异常:{}",e.getMessage());
            return file;
        }finally {
            if (p != null) {
                // 关闭进程
                p.destroy();
            }
            if (connection != null && connection.isConnected()){
                connection.disconnect();
            }
        }
        return pdfFile;
    }


}

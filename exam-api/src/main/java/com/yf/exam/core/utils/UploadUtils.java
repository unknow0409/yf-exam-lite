package com.yf.exam.core.utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

/**
 * 上传文件工具类
 */
public class UploadUtils {
	
	private static final Logger log = LoggerFactory.getLogger(UploadUtils.class);
	
	/**
	 * 
	 * @param file  上传的文件
	 * @param dir   保存的目录分类
	 * @param uploadPath  服务器保存上传文件的根目录
	 * @return
	 */
	public static String saveFile(MultipartFile file, String dir, String uploadPath) {
        try {
            if (file.isEmpty()) {
                return null;
            }
            // 获取文件名
            String fileName = file.getOriginalFilename();
            log.debug("上传的文件名为：" + fileName);
            
            // 设置文件存储路径
            String newName = UUID.randomUUID().toString();
            // 获取文件的后缀名
        	String suffixName = fileName.substring(fileName.lastIndexOf("."));
            log.debug("文件的后缀名为：" + suffixName);
        	newName += suffixName;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
            String mediaPath = "/" + dir + "/" + sdf.format(new Date()) + "/" + newName;
            String path = uploadPath + mediaPath ;
            log.debug("文件的存储的名称为：" + newName);
            File dest = new File(path);
            // 检测是否存在目录
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdirs();// 新建文件夹
            }
            file.transferTo(dest);// 文件写入
            
            return mediaPath;
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    	return null;
    }


    //方式一: 后缀名进行判断
    //方式二: file.getContentType()--MIME类型进行判断
    /**
     * 判断是否为图片
     * @param file
     * @return
     */
    public static Boolean fileCompareImg(MultipartFile file){

        String suffixName = UploadUtils.suffixName(file);
        if(suffixName.equals(".jpg") || suffixName.equals(".jpeg") ||
                suffixName.equals(".png"))
	        return true;
        return false;
	}

    /**
     * 判断是否为 office
     * @param file
     * @return
     */
    public static Boolean fileCompareOffice(MultipartFile file){

        String suffixName = UploadUtils.suffixName(file);
        if(suffixName.equals(".doc") || suffixName.equals(".xls") ||
                suffixName.equals(".ppt") ||suffixName.equals(".docx") ||
        suffixName.equals(".xlsx") || suffixName.equals(".pptx"))
            return true;
        return false;
    }

    /**
     * 判断是否为 pdf
     * @param file
     * @return
     */
    public static Boolean fileComparePdf(MultipartFile file){

        String suffixName = UploadUtils.suffixName(file);
        if(suffixName.equals(".pdf"))
            return true;
        return false;
    }

    /**
     * 判断是否为 视频
     * @param file
     * @return
     */
    public static Boolean fileCompareVideo(MultipartFile file){

        String suffixName = UploadUtils.suffixName(file);
        if(suffixName.equals(".avi") || suffixName.equals(".wmv") ||
                suffixName.equals(".mp4")||suffixName.equals("mkv")||
                    suffixName.equals(".ogv")||suffixName.equals(".flv")||
                        suffixName.equals(".rm")||suffixName.equals(".rmvb"))
            return true;
        return false;
    }


    /**
     * 获取文件后缀
     * @param file
     * @return
     */
    public static String suffixName(MultipartFile file){
        String fileName = file.getOriginalFilename();
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        return suffixName;
    }


}

package com.yf.exam.core.utils.video;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * java实现视频格式的转化
 *
 * @author liuyazhuang
 */
public class ChangeVideo {


    // 检查文件是否存在
    private static Boolean checkfile(String path) {
        File file = new File(path);

        if (!file.isFile()) {
            return false;
        }
        return true;
    }

    /**
     * @param inputFile:需要转换的视频
     * @param outputFile：转换后的视频
     */
    public static boolean convert(String inputFile, String outputFile) {
        if (!checkfile(inputFile)) {
            System.out.println(inputFile + " is nokt file");
            return false;
        }
        if (process(inputFile, outputFile)) {

            System.out.println("ok");
            return true;
        }
        return false;
    }



    /**
     * @return 转换视频文件
     */
    private static boolean process(String inputFile, String outputFile) {

        int type = checkContentType(inputFile);
        boolean status = false;

        if (type == 0) {

            System.out.println("直接转成mp4格式");
            //执行开始
            long startTime = System.currentTimeMillis();
            // 直接将文件转为MP4文件
            status = processMP4(inputFile, outputFile);
            //执行结束
            long endTime = System.currentTimeMillis();
            //执行时长
            float excTime = (float) (endTime - startTime) / 1000;
            System.out.println("执行时长" + excTime + "s");

        } else if (type == 1) {

            System.out.println("mencoder转换AVI");
            //执行开始
            long startTime = System.currentTimeMillis();
            //执行
            String avifilepath = processAVI(type, inputFile);

            if (avifilepath == null) {
                System.out.println("avi文件没有得到");
                return false;// avi文件没有得到
            }

            System.out.println("AVI转换MP4");
            status = processMP4(avifilepath, outputFile);// 将avi转为flv

            long endTime = System.currentTimeMillis();
            //执行时长
            float excTime = (float) (endTime - startTime) / 1000;
            System.out.println("执行时长" + excTime + "s");
        }
        return status;
    }

    private static int checkContentType(String inputFile) {

        String type = inputFile.substring(inputFile.lastIndexOf(".") + 1, inputFile.length()).toLowerCase();

        if (type.equals("avi")) {
            return 0;
        } else if (type.equals("mpg")) {
            return 0;
        } else if (type.equals("wmv")) {
            return 0;
        } else if (type.equals("3gp")) {
            return 0;
        } else if (type.equals("mov")) {
            return 0;
        } else if (type.equals("mp4")) {
            return 0;
        } else if (type.equals("asf")) {
            return 0;
        } else if (type.equals("asx")) {
            return 0;
        } else if (type.equals("flv")) {
            return 0;
        }
        else if (type.equals("wmv9")) {
            return 1;
        } else if (type.equals("rm")) {
            return 1;
        } else if (type.equals("rmvb")) {
            return 1;
        }
        return 9;
    }

    /**
     * ffmpeg能解析的格式：（asx，asf，mpg，wmv，3gp，mp4，mov，avi，flv等）直接转换为目标视频
     */
    private static boolean processMP4(String inputFile, String outputFile) {

        if (!checkfile(inputFile)) {
            System.out.println(inputFile + " is not file");
            return false;
        }

        List<String> commend = new ArrayList<String>();

        commend.add(Constants.ffmpegPath);
        commend.add("-i");
        commend.add(inputFile);
        commend.add("-c:v");
        commend.add("libx264");
        commend.add("-mbd");
        commend.add("0");
        commend.add("-c:a");
        commend.add("aac");
        commend.add("-strict");
        commend.add("-2");
        commend.add("-pix_fmt");
        commend.add("yuv420p");
        commend.add("-movflags");
        commend.add("faststart");
        commend.add(outputFile);

        try {

            Process videoProcess = new ProcessBuilder(commend).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;

        } catch (Exception e) {

            e.printStackTrace();
            return false;

        }
    }

    /**
     * 对ffmpeg无法解析的文件格式(wmv9，rm，rmvb等),可以先用别的工具（mencoder）转换为avi(ffmpeg能解析的)格式.
     */
    private static String processAVI(int type, String inputFile) {
        File file = new File(Constants.avifilepath);

        if (file.exists()){
            file.delete();
        }

        List<String> commend = new ArrayList<String>();
        commend.add(Constants.mencoderPath);
        commend.add(inputFile);
        commend.add("-oac");
        commend.add("lavc");
        commend.add("-lavcopts");
        commend.add("acodec=mp3:abitrate=64");
        commend.add("-ovc");
        commend.add("xvid");
        commend.add("-xvidencopts");
        commend.add("bitrate=600");
        commend.add("-of");
        commend.add("mp4");
        commend.add("-o");
        commend.add(Constants.avifilepath);

        try {

            ProcessBuilder builder = new ProcessBuilder();
            Process process = builder.command(commend).redirectErrorStream(true).start();
            new PrintStream(process.getInputStream());
            new PrintStream(process.getErrorStream());
            process.waitFor();
            return Constants.avifilepath;

        } catch (Exception e) {

            e.printStackTrace();
            return null;

        }
    }

    static class PrintStream extends Thread {
        java.io.InputStream __is = null;

        public PrintStream(java.io.InputStream is) {
            __is = is;
        }

        public void run() {
            try {
                while (this != null) {
                    int _ch = __is.read();
                    if (_ch != -1)
                        System.out.print((char) _ch);
                    else break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}